package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

/**
 * Created by root on 11/26/16.
 */

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnConfirm,btnConfirmOTP;
    private EditText edtHomeID,edtOTP;
    private ProgressDialog progressDialog;
    private LinearLayout llConfirmOtp;
    private TextView tvName;
    HashMap<String,String> mMapResult=new HashMap<>();
    HashMap<String,String> userMap=new HashMap<>();
    SwitchSessionManager manager;
    String response=null;
    Utils req=new Utils();
    int panelNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        initControl();
        manager.setTopicName("192.168.0.119");
        //getIpAddress();


        if(Utils.getSharedPValue(this,Utils.HOME_FLAG).equals("1")){
            finish();
            Intent i=new Intent(this,MainActivity.class);
            startActivity(i);
        }

//        Utils.saveSharedP(this,Utils.HOMEID,"gtx11");
//        finish();
//        Intent i=new Intent(this,MainActivity.class);
//        startActivity(i);



    }

    private void initControl() {

        edtHomeID= (EditText) findViewById(R.id.edtHomeID);
        edtOTP= (EditText) findViewById(R.id.edtOTP);
        btnConfirm= (Button) findViewById(R.id.btnConfirm);
        btnConfirmOTP= (Button) findViewById(R.id.btnConfirmOTP);
        llConfirmOtp= (LinearLayout) findViewById(R.id.llConfirmOtp);
        tvName= (TextView) findViewById(R.id.tvName);

        manager=new SwitchSessionManager(this);

        btnConfirm.setOnClickListener(this);
        llConfirmOtp.setOnClickListener(this);
        btnConfirmOTP.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btnConfirm:
                onClickOfConfirmButton();
                break;
            case R.id.btnConfirmOTP:
                onClickOfConfirmOTP();
                break;
        }

    }


    private void onClickOfConfirmOTP() {

        if(mMapResult.get("otpcode").equals(edtOTP.getText().toString())) {

            Utils.saveSharedP(this,Utils.HOMEID,edtHomeID.getText().toString()); //device id
            finish();
            Intent intent = new Intent(this, SelectWifi.class);
            intent.putExtra("LoginInfo", mMapResult);
            startActivity(intent);

        }else{
            Toast.makeText(this, "Please enter valid OTP.", Toast.LENGTH_SHORT).show();
        }
    }

    private void onClickOfConfirmButton() {

        if(edtHomeID.getText().toString().equals("")){
            Toast.makeText(this, "Please enter HomeID.", Toast.LENGTH_SHORT).show();
            return;
        }


        if(Utils.isInternetAvailable(this)) {
            new AsyncVerifyHomeID().execute(edtHomeID.getText().toString());
        }else{
            Toast.makeText(this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }
    private class AsyncVerifyHomeID extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= new ProgressDialog(HomeActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response=null;
            Utils req=new Utils();
            try{
                JSONObject jObj=new JSONObject();
                jObj.put("homeId",params[0]);
                response=req.doPostRequest(Utils.URL_GENIE_AWS+"/home/homeverification",jObj+"");
               //http://192.168.0.4:6060
                Log.d("login_confirm","obj== "+jObj);
                Log.d("login_confirm","url== "+Utils.URL_GENIE_AWS+"/home/homeverification");

                Log.d("login_confirm","result== "+response);
              //  String url="http://gsmarthome.genieiot.in/home/homeverification";

               // response=req.doPostRequest(url,jObj+"");
            }
            catch(Exception e){
                Log.d("Exception",e.toString());
            }

            return response;
        }
;
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if(progressDialog!=null){
                progressDialog.cancel();
            }

            parseJSONResponse(result);
        }
    }
    private void parseJSONResponse(String result) {
        if(result!=null){
            Log.d("Verification -->",result);
            try {
                JSONObject jObj = new JSONObject(result);
                if(jObj.getString("status").equals("SUCCESS")) {

                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    String HOMEID=jObj.getString("homeId");
                    manager.setHomeID(HOMEID);

                    JSONObject jConfig=new JSONObject(jObj.getString("deviceDetails"));

                    mMapResult.put("deviceId",jConfig.getString("deviceId"));
                    mMapResult.put("userId",jConfig.getString("adminUser"));
                    mMapResult.put("username",jObj.getString("username"));
                    mMapResult.put("otpcode",jObj.getString("otpcode"));

                    llConfirmOtp.setVisibility(View.VISIBLE);
                    tvName.setText(jObj.getString("username"));

                    edtOTP.setText(jObj.getString("otpcode"));

                    JSONObject jUserDetails=new JSONObject(jObj.getString("userDetails"));
                    Log.d("User Deatils",""+jUserDetails);
                    manager.setUSERDETAILS(jUserDetails+"");

                    String deviceID=jConfig.getString("deviceId");
                    manager.setDeviceId(deviceID);


                }
                else {
                    Toast.makeText(HomeActivity.this,"Please check homeId", Toast.LENGTH_SHORT).show();
                }

            }catch(Exception e){}
        }else{

            Toast.makeText(HomeActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public void getIpAddress(){
        try {
            Enumeration nis = NetworkInterface.getNetworkInterfaces();
            while(nis.hasMoreElements())
            {
                NetworkInterface ni = (NetworkInterface) nis.nextElement();
                Enumeration ias = ni.getInetAddresses();
                while (ias.hasMoreElements())
                {
                    InetAddress ia = (InetAddress) ias.nextElement();
                    System.out.println(ia.getHostAddress());
                }

            }
        } catch (SocketException ex) {
            Logger.getLogger(HomeActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
