package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

import static gsmartadmin.genieiot.com.myapplication.Utils.Utils.URL_GENIE_AWS;

/**
 * Created by root on 23/1/17.
 */

public class SendConfiguration extends AppCompatActivity implements View.OnClickListener {

    Button btnSendConfiguration;
    private ArrayList<HashMap<String, String>> panelList;
    ProgressDialog progressDialog;
    SwitchSessionManager manager;
    private static final String TAG = "SendConfiguration";
    int last_Switch_Id, last_Switch_Id1;
    String response = null;
    Utils req = new Utils();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sendconfiguration);
        initializeControls();

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean wifiStatus = wifiManager.isWifiEnabled();
        Log.d(TAG, "is wifi on/off: " + wifiStatus);
        if (!wifiStatus) {
            wifiManager.setWifiEnabled(true);
        } else {
            // new Lastproductid().execute();
        }
    }

    private void initializeControls() {
        btnSendConfiguration = (Button) findViewById(R.id.btnSendConfiguration);
        btnSendConfiguration.setOnClickListener(this);
        manager = new SwitchSessionManager(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendConfiguration:

                new Lastproductid().execute();


        }

    }

    private void sendConfiguration() {
        DatabaseHandler database = new DatabaseHandler(SendConfiguration.this);
        panelList = database.getAllHomeInfo();

        String mPanelList = "";
        String switchName = "";
        String str_switchList = "";

        //panel5-BD1-15-123456-FEcBF-L000-GR-1
        last_Switch_Id1 = last_Switch_Id;
        for (int pn = 0; pn < panelList.size(); pn++) {
            if (pn == panelList.size() - 1) {

                mPanelList = mPanelList + panelList.get(pn).get(DatabaseHandler.TOPIC) + "-" + panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS) + "-" + panelList.get(pn).get(DatabaseHandler.CURTAINVALUE) + "-" + panelList.get(pn).get(DatabaseHandler.IOTPRODSHORTCUT) + "-" + panelList.get(pn).get(DatabaseHandler.LOCKVALUE);

                Log.d(TAG, "PANELLIST1==" + mPanelList);
            } else {
                mPanelList = mPanelList + panelList.get(pn).get(DatabaseHandler.TOPIC) + "-" + panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS) + "-" + panelList.get(pn).get(DatabaseHandler.CURTAINVALUE) + "-" + panelList.get(pn).get(DatabaseHandler.IOTPRODSHORTCUT) + "-" + panelList.get(pn).get(DatabaseHandler.LOCKVALUE) + ",";

                Log.d(TAG, "PANELLIST2==" + mPanelList);
            }

            String switchDetails = panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS);
            if (switchDetails.contains("-")) {
                //BD1-57-103000-offic
                String[] switchDetailArr = switchDetails.split("-");

                if (switchDetailArr.length > 2) {
                    char[] selectedSwitch = switchDetailArr[2].toCharArray();

                    for (char c : selectedSwitch) {
                        if (c != '0') {
                            if (str_switchList.isEmpty()) {
                                str_switchList = str_switchList + (last_Switch_Id1 + 1);
                            } else {
                                str_switchList = str_switchList + "," + (last_Switch_Id1 + 1);
                            }
                            last_Switch_Id1 = last_Switch_Id1 + 1;
                        }
                    }
                }
            }

        }
        Log.d(TAG, "str_switchList: " + str_switchList);

        SwitchSessionManager session = new SwitchSessionManager(SendConfiguration.this);
        String mRouter = session.getPSSID() + "," + session.getPPASSWORD();
        String mSecure = session.getSSSID() + "," + session.getSPASSWORD();

        String mFinal = "";
        for (int pp = 0; pp < panelList.size(); pp++) {
            mFinal = mFinal + panelList.get(pp).get(DatabaseHandler.SWITCHDETAILS) + "," + mRouter + "," + mSecure + "," + panelList.get(pp).get(DatabaseHandler.PANELDEATILS) + ",";
            Log.d(TAG, "ConfigDetails: " + mFinal);
        }

        Log.d(TAG, "PANELLIST==" + mPanelList);
        Log.d(TAG, "PANELLIST FINAL==" + mFinal);


        if (panelList.size() > 0) {
            new AsynConfigureFile().execute(mPanelList, mFinal, str_switchList);
        } else {
            Toast.makeText(this, "Fill the Panel.", Toast.LENGTH_SHORT).show();
        }
    }

    private class AsynConfigureFile extends AsyncTask<String, Void, String> {
        String response = "";
        String response1 = "";
        String jBody;
        Utils utils = new Utils();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SendConfiguration.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                jBody = createJSONBody(params[0], params[1], params[2]);
                Log.d(TAG, "JSON Body" + jBody);
                response = utils.doPostRequest(Utils.URL_GENIE + "/home/fillPanel", jBody + "");
                response1 = utils.doPostRequest(Utils.URL_GENIE_AWS + "/home/fillPanel", jBody + "");    //BY ME

                Log.d(TAG, "Fill_Panel_response LOCAL: url== " + Utils.URL_GENIE + "/home/fillPanel");
                Log.d(TAG, "Fill_Panel_response LOCAL: jbody== " + jBody);
                Log.d(TAG, "Fill_Panel_response LOCAL:== " + response);
//
               /* Log.d(TAG, "Fill_Panel_response CENTRAL: url== " + Utils.URL_GENIE_AWS + "/home/fillPanel");
                Log.d(TAG, "Fill_Panel_response: CENTRAL jbody==" + jBody);
                Log.d(TAG, "Fill_Panel_response: CENTRAL==" + response1);*/
            } catch (Exception e) {
            }
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            if (result != null) {
                Log.d(TAG, "url===" + Utils.URL_GENIE + "/home/fillPanel");
                Log.d(TAG, "response===" + result);

                try {
                    JSONObject jObj = new JSONObject(result);
                    if (jObj.getString("status").equals("SUCCESS")) {
                        JSONObject jObj1 = new JSONObject(response1);
                        if (jObj1.getString("status").equals("SUCCESS")) {
                            Log.d(TAG, "Fill_Panel_response CENTRAL: url== " + Utils.URL_GENIE_AWS + "/home/fillPanel");
                            Log.d(TAG, "Fill_Panel_response: CENTRAL jbody==" + jBody);
                            Log.d(TAG, "Fill_Panel_response: CENTRAL==" + response1);

                            Toast.makeText(SendConfiguration.this, "Sync Config. file successfully.", Toast.LENGTH_SHORT).show();

                            new Lastproductid().execute();

                        } else {

                        }
                        Toast.makeText(SendConfiguration.this, "Sync Config. file successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SendConfiguration.this, "Fail to sync the Config. file.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private String createJSONBody(String strPanelList, String strIpConfig, String str_switchList) {
        JSONObject jObj = new JSONObject();
        String USER_DETAILS = manager.getUSERDETAILS();
        DatabaseHandler db = new DatabaseHandler(SendConfiguration.this);
        String mTowerString = db.getGenieTower();

        try {

            JSONObject jUserDetailsObj = new JSONObject(USER_DETAILS);
            Log.d(TAG, "All Data" + jUserDetailsObj);

            jObj.put("panellist", strPanelList);
            jObj.put("ipConfigration", strIpConfig);
            jObj.put("towerInfo", mTowerString);

            jObj.put("id", jUserDetailsObj.getString("id"));
            jObj.put("lastName", jUserDetailsObj.getString("lastName"));
            jObj.put("phoneNumber", jUserDetailsObj.getString("phoneNumber"));
            jObj.put("email", jUserDetailsObj.getString("email"));
            jObj.put("deviceType", jUserDetailsObj.getString("deviceType"));
            jObj.put("image", jUserDetailsObj.getString("image"));
            jObj.put("firstName", jUserDetailsObj.getString("firstName"));
            jObj.put("password", jUserDetailsObj.getString("password"));
            jObj.put("deviceId", manager.getDeviceId());
            jObj.put("userType", jUserDetailsObj.getString("userType"));
            jObj.put("homeId", manager.getHOMEID());
            jObj.put("isEmailVerified", jUserDetailsObj.getString("isEmailVerified"));
            jObj.put("isFirstLogin", jUserDetailsObj.getString("isFirstLogin"));
            jObj.put("birthDate", jUserDetailsObj.getString("birthDate").replaceAll("\\\\", ""));
            jObj.put("switchList", str_switchList);

            Log.d(TAG, "JSON DATA" + jObj);

        } catch (Exception e) {
        }

        return jObj + "";

    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private class Lastproductid extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SendConfiguration.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            try {
                JSONObject jObj = new JSONObject();
                jObj.put("homeId", manager.getHOMEID());

                //{"last_iot_product_number":"55","total_room_size":9,"last_Switch_Id":234}
                response = req.doPostRequest(URL_GENIE_AWS + "/home/getLastIotProductNumber", "" + jObj);
                Log.d(TAG, "url: getLastIotProductNumber, response ===" + response);
                JSONObject jResult = new JSONObject(response);
                Object last_switch_id = jResult.get("last_Switch_Id");
                last_Switch_Id = Integer.parseInt(last_switch_id.toString());
                last_Switch_Id1 = last_Switch_Id;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "error===" + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(TAG, "error====" + e.getMessage());
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.cancel();
                    }
                }
            });
            // Log.d("Get_All_Data ","URL==" +"http://192.168.0.2:6060/home/getLastIotProductNumber==="+response);
            return last_Switch_Id;
        }

        @Override
        protected void onPostExecute(Integer last_Switch_Id) {
            super.onPostExecute(last_Switch_Id);
            sendConfiguration();
        }
    }

}
