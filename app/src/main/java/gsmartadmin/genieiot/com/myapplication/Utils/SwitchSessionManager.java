package gsmartadmin.genieiot.com.myapplication.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by root on 9/21/16.
 */

public class SwitchSessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    public static String SW1 = "sw1";
    public static String SW2 = "sw2";
    public static String SW3 = "sw3";
    public static String SW4 = "sw4";
    public static String SW5 = "sw5";
    public static String SW6 = "sw6";


    public static String PSSID="pssid";
    public static String PPASSWORD="ppassword";
    public static String SSSID="sssid";
    public static String SPASSWORD="spassword";

    public static String ID="id";
    public static String First_Name="firstname";
    public static String Last_Name="lastname";
    public static String EMAIL="email";
    public static String PASSWORD="password";
    public static String PHONE_NUMBER="phonenumber";
    public static String USER_TYPE="usertype";
    public static String IMAGE="image";
    public static String DEVICE_ID="deviceid";
    public static String DEVICE_TYPE="devicetype";
    public static String USERDETAILS="userdetails";
    public static String IOTPRODShortcut="iotprodtype";
    public static String HOMEID="homeid";
    public static String MAINLOCK="mainlock";
    public static String LOCAL_IP="local_ip";
    // Context
    Context _context;

    // Constructor
    public SwitchSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("GSmartHome",0);
        editor = pref.edit();
    }
    public void saveUserDetails(String id,String fname,String lname,String email,String password,String phone,String usertype,String image,String deviceId,String deviceType)
    {
        //store user details
        editor.putString(ID,id);
        editor.putString(First_Name,fname);
        editor.putString(Last_Name,lname);
        editor.putString(EMAIL,email);
        editor.putString(PASSWORD,password);
        editor.putString(PHONE_NUMBER,phone);
        editor.putString(USER_TYPE,usertype);
        editor.putString(IMAGE,image);
        editor.putString(DEVICE_ID,deviceId);
        editor.putString(DEVICE_TYPE,deviceType);

    }

    public void saveHomeDetails(String pssid,String ppassword,String sssid,String spassword){
        editor.putString(PSSID,pssid);
        editor.putString(PPASSWORD,ppassword);
        editor.putString(SSSID,sssid);
        editor.putString(SPASSWORD,spassword);
        editor.commit();
    }

    public void saveSwitch(String sw1, String sw2, String sw3, String sw4, String sw5, String sw6) {
        // Storing login value as TRUE
        editor.putString(SW1,sw1);
        editor.putString(SW2,sw2);
        editor.putString(SW3,sw3);
        editor.putString(SW4,sw4);
        editor.putString(SW5,sw5);
        editor.putString(SW6,sw6);
        editor.commit();
    }




    public String getSW1(){
      return pref.getString(SW1,"");
    }

    public String getSW2(){
        return pref.getString(SW2,"");
    }

    public String getSW3(){
        return pref.getString(SW3,"");
    }

    public String getSW4(){
        return pref.getString(SW4,"");
    }

    public String getSW5(){
        return pref.getString(SW5,"");
    }

    public String getSW6(){
        return pref.getString(SW6,"");
    }

    public   String getIOTPRODShortcut() {
        return pref.getString(IOTPRODShortcut,"");
    }
    public void setIOTPRODShortcut(String iotProdShortcut)
    {
        editor.putString(IOTPRODShortcut,iotProdShortcut);
        editor.commit();
    }

    public  String getUSERDETAILS() {
        return pref.getString(USERDETAILS,"");
    }
    public void setUSERDETAILS(String userdetails)

    {
        editor.putString(USERDETAILS,userdetails);
        editor.commit();

    }
    public String getDeviceId()
    {
        return pref.getString(DEVICE_ID,"");
    }

    public  void setDeviceId(String deviceId) {
        editor.putString(DEVICE_ID,deviceId);
        editor.commit();

    }

    public void setHomeID(String homeid)
    {
        editor.putString(HOMEID,homeid);
        editor.commit();
    }
    public String getHOMEID()
    {
        return pref.getString(HOMEID,"");
    }

    public  void setMAINLOCK(String mainlock) {
        editor.putString(MAINLOCK,mainlock);
        editor.commit();
    }

    public  String getMAINLOCK() {
        return pref.getString(MAINLOCK,"");
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    public String getPSSID(){
        return pref.getString(PSSID,"");
    }

    public String getPPASSWORD(){
        return pref.getString(PPASSWORD,"");
    }

    public String getSSSID(){
        return pref.getString(SSSID,"");
    }

    public String getSPASSWORD(){
        return pref.getString(SPASSWORD,"");
    }

    public String getTopicName() {
        return pref.getString(LOCAL_IP,"");
    }

    public void setTopicName(String mStr) {
        editor.putString(LOCAL_IP,mStr);
        editor.commit();
    }


}

