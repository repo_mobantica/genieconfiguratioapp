package gsmartadmin.genieiot.com.myapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gsmartadmin.genieiot.com.myapplication.Adapter.CustomSpinnerAdapter;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

/**
 * Created by root on 11/11/16.
 */

public class SelectWifi extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private WifiManager mainWifi;
    private WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
    private Spinner spSelectWifi;
    private EditText edtWifiPassword, edtsecureSSID, edtSecurePassword;
    private Button btnSubmit;
    private String mSSIDName, mPassword;
    private String mSecurePassword, mSecureSSID;

    public static SelectWifi thisInstance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_wifi_layout);
        initControl();
        startWifiScanning();
    }


    private void initControl() {

        spSelectWifi = (Spinner) findViewById(R.id.spSelectWifi);
        edtWifiPassword = (EditText) findViewById(R.id.edtWifiPassword);
        edtsecureSSID = (EditText) findViewById(R.id.edtsecureSSID);
        edtSecurePassword = (EditText) findViewById(R.id.edtSecurePassword);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(this);
        spSelectWifi.setOnItemSelectedListener(this);

    }

    private void startWifiScanning() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!permissionsGranted()) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
            }
        }

        // Initiate wifi service manager
        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (mainWifi.isWifiEnabled() == false) {
            // If wifi disabled then enable it
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }
        // wifi scaned value broadcast receiver
        receiverWifi = new WifiReceiver();
        // Register broadcast receiver
        // Broacast receiver will automatically call when number of wifi connections changed
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        mainWifi.startScan();

    }

    private Boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                mPassword = edtWifiPassword.getText().toString();
                mSecurePassword = edtSecurePassword.getText().toString();
                mSecureSSID = edtsecureSSID.getText().toString();

                if (edtWifiPassword.getText().toString().length() < 8) {
                    //Toast.makeText(this,"Please enter 8 digit  password.", Toast.LENGTH_SHORT).show();
                    edtWifiPassword.setError("Please enter 8 digit password");
                    return;
                }

                if (edtsecureSSID.getText().toString().length() < 1) {
                    //Toast.makeText(this,"Please enter valid SSID.", Toast.LENGTH_SHORT).show();
                    edtsecureSSID.setError("Please enter valid SSID");

                    return;
                }
                if (edtSecurePassword.getText().toString().length() < 8) {
                    edtSecurePassword.setError("Please enter 8 digit backup hotspot password");

                    // Toast.makeText(this,"Please enter 8 digit secure password.", Toast.LENGTH_SHORT).show();
                    return;
                }

                AlertDialogWifiDetails();

//                SwitchSessionManager session=new SwitchSessionManager(this);
//                session.saveHomeDetails(mSSIDName,mPassword,mSecureSSID,mSecurePassword);

                // finish();
//                Intent intent=new Intent(this,MainActivity.class);
//                startActivity(intent);
//                Utils.saveSharedP(this,Utils.HOME_FLAG,"1");

                break;

        }
    }

    private void AlertDialogWifiDetails() {

        final Dialog dialog = new Dialog(SelectWifi.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.wifidetails);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);

        final TextView wifiName = (TextView) dialog.findViewById(R.id.wifiName);
        final TextView wifiPassword = (TextView) dialog.findViewById(R.id.wifiPassword);
        wifiPassword.setText(edtWifiPassword.getText().toString());
        wifiName.setText(mSSIDName);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SwitchSessionManager session = new SwitchSessionManager(SelectWifi.this);
                session.saveHomeDetails(mSSIDName, mPassword, mSecureSSID, mSecurePassword);
                Intent intent = new Intent(SelectWifi.this, MainActivity.class);
                startActivity(intent);
                Utils.saveSharedP(SelectWifi.this, Utils.HOME_FLAG, "1");
                finish();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spSelectWifi) {
            HashMap<String, String> selectedItem = (HashMap<String, String>) parent.getItemAtPosition(position);
            mSSIDName = selectedItem.get("NAME");
            // Toast.makeText(this,""+mSSIDName, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    class WifiReceiver extends BroadcastReceiver {
        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            new WifiAsyncTask().execute();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //setLocationSettingOnOff();
        thisInstance = this;
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        thisInstance = null;
        unregisterReceiver(receiverWifi);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void setLocationSettingOnOff() {

        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Services Not Active");
            builder.setMessage("Please enable Location Services and GPS");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

    private class WifiAsyncTask extends AsyncTask<Void, Void, ArrayList<HashMap<String, String>>> {

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(Void... params) {
            sb = new StringBuilder();
            wifiList = mainWifi.getScanResults();
            sb.append("\n Number Of Wifi connections :" + wifiList.size() + "\n\n");
            ArrayList<HashMap<String, String>> ArraList = new ArrayList<>();
            for (int i = 0; i < wifiList.size(); i++) {
                HashMap<String, String> map = new HashMap<>();
                sb.append(new Integer(i + 1).toString() + ". ");
                sb.append((wifiList.get(i)).toString());
                sb.append("\n\n");
                map.put("NAME", (wifiList.get(i)).SSID);
                map.put("ID", i + "");
                ArraList.add(map);

            }
            //new code to get IP address of devices
            WifiInfo wifiinfo = mainWifi.getConnectionInfo();
            byte[] myIPAddress = BigInteger.valueOf(wifiinfo.getIpAddress()).toByteArray();
            InetAddress myInetIP = null;
            try {
                myInetIP = InetAddress.getByAddress(myIPAddress);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            String myIP = myInetIP.getHostAddress();
            Log.d("ID Address Of Devices:", myIP);
            //end of the code
            return ArraList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> ArraList) {
            super.onPostExecute(ArraList);
            if (thisInstance != null) {
                CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(SelectWifi.this, ArraList);
                spSelectWifi.setAdapter(customSpinnerAdapter);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 123:
                mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                if (mainWifi.isWifiEnabled() == false) {
                    // If wifi disabled then enable it
                    Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
                    mainWifi.setWifiEnabled(true);
                }
                // wifi scaned value broadcast receiver
                receiverWifi = new WifiReceiver();
                // Register broadcast receiver
                // Broacast receiver will automatically call when number of wifi connections changed
                registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                mainWifi.startScan();
                break;
            default:
                break;
        }
    }
}
