package gsmartadmin.genieiot.com.myapplication.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 11/28/16.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "com.genie.database";

    public static final String HOMEID="homeid";
    private static final String TABLE_HOME="HomeInfo";
    private static final String TABLE_ROOMTYPE="RoomInfo";
    private static final String TABLE_SWITCHINFO="SwitchInfo";

    public static final String ROOMID="RoomId";
    public static final String ROOMNAME="RoomName";
    public static final String SWITCHID="SwitchID";
    public static final String SWITCHNAME="SwitchName";
    public static final String KEY_ID ="id" ;
    public static final String SWITCHIP ="SwitchIP";
    public static final String TOPIC ="Topic";
    public static final String SWITCHDETAILS ="switch_details";
    public static final String PANELDEATILS ="panel_details";
    public static final String IOTPRODSHORTCUT ="iot_prod_shortcut";
    public static final String IOTPRODNAME="iot_prod_name";
    public static final String CURTAINVALUE ="curtainvalue";
    public static final String LOCKVALUE="lockvalue";
   // public static final String SWITCHLIST="switchList";

    public static String SW1 = "sw1";
    public static String SW2 = "sw2";
    public static String SW3 = "sw3";
    public static String SW4 = "sw4";
    public static String SW5 = "sw5";
    public static String SW6 = "sw6";

    public static String SW1_ID = "sw1id";
    public static String SW2_ID = "sw2id";
    public static String SW3_ID = "sw3id";
    public static String SW4_ID = "sw4id";
    public static String SW5_ID = "sw5id";
    public static String SW6_ID = "sw6id";

    public static String ROOM_LOCATION="Room_Location";

    public static String TABLE_TOWER="Table_tower";
    public static String TOWER_ID="tower_id";
    public static String TOWER_NAME="tower_name";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_HOME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + HOMEID + " TEXT,"
                + ROOMID + " TEXT ," +ROOMNAME+" TEXT, "+SW1_ID+" TEXT, "+SW1+" TEXT , "+
                SW2_ID+" TEXT, "+SW2+" TEXT , "+SW3_ID+" TEXT, "+SW3+" TEXT , "+
                SW4_ID+" TEXT, "+SW4+" TEXT , "+SW5_ID+" TEXT, "+SW5+" TEXT , "+
                SW6_ID+" TEXT, "+SW6+" TEXT , "+ SWITCHIP+" TEXT ," + ROOM_LOCATION+" TEXT,"+TOPIC+" TEXT ,"+SWITCHDETAILS+" TEXT,"+PANELDEATILS+" TEXT,"+IOTPRODSHORTCUT+" TEXT,"+CURTAINVALUE+" TEXT,"+IOTPRODNAME+" TEXT,"+LOCKVALUE+" TEXT"+")";

        String CREATE_TOWER_TABLE="CREATE TABLE "+ TABLE_TOWER+" ("+TOWER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+TOWER_NAME+" TEXT"+")";

        db.execSQL(CREATE_CONTACTS_TABLE);
        db.execSQL(CREATE_TOWER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_HOME);
        onCreate(db);
    }

    public void addHomeInfo(ArrayList<HashMap<String,String>> switchMap,HashMap<String,String> mMap) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(HOMEID, mMap.get(HOMEID));
        values.put(ROOMID, mMap.get(ROOMID));
        values.put(ROOMNAME, mMap.get(ROOMNAME));

        values.put(SW1_ID, switchMap.get(0).get("ID"));
        values.put(SW1,switchMap.get(0).get("NAME"));

        values.put(SW2_ID,switchMap.get(1).get("ID"));
        values.put(SW2, switchMap.get(1).get("NAME"));

        values.put(SW3_ID, switchMap.get(2).get("ID"));
        values.put(SW3,switchMap.get(2).get("NAME"));

        values.put(SW4_ID,switchMap.get(3).get("ID"));
        values.put(SW4, switchMap.get(3).get("NAME"));

        values.put(SW5_ID,switchMap.get(4).get("ID"));
        values.put(SW5,switchMap.get(4).get("NAME"));

        values.put(SW6_ID, switchMap.get(5).get("ID"));
        values.put(SW6,switchMap.get(5).get("NAME"));

        values.put(SWITCHIP,mMap.get(SWITCHIP));
        values.put(ROOM_LOCATION,mMap.get(ROOM_LOCATION));
        values.put(TOPIC,mMap.get(TOPIC));
        values.put(SWITCHDETAILS,mMap.get(SWITCHDETAILS));
        values.put(PANELDEATILS,mMap.get(PANELDEATILS));
        values.put(IOTPRODSHORTCUT,mMap.get(IOTPRODSHORTCUT));
        values.put(CURTAINVALUE,mMap.get(CURTAINVALUE));
        values.put(IOTPRODNAME,mMap.get(IOTPRODNAME));
        values.put(LOCKVALUE,mMap.get(LOCKVALUE));
       // values.put(SWITCHLIST,mMap.get(SWITCHLIST));

        // Inserting Row
        try {
            long s = db.insert(TABLE_HOME, null, values);
            Log.d("Switch Insert ","Success "+mMap.get(IOTPRODNAME));

        }catch(Exception e){
            Log.d("Switch Insert ","Fail");
        }
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public void addGenieTower() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TOWER_NAME,"Genie Tower");
        try {
            long s = db.insert(TABLE_TOWER, null, values);
            Log.d("Switch Insert ","Success "+values);
        }catch(Exception e){
            Log.d("Switch Insert ","Fail");
        }

        db.close();
    }

    public ArrayList<HashMap<String,String>> getAllHomeInfo() {
        ArrayList<HashMap<String,String>> pannelList = new ArrayList<HashMap<String,String>>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HOME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                HashMap<String,String> mMap=new HashMap<>();
                mMap.put(KEY_ID,cursor.getString(cursor.getColumnIndex(KEY_ID)));
                mMap.put(HOMEID,cursor.getString(cursor.getColumnIndex(HOMEID)));
                mMap.put(ROOMID,cursor.getString(cursor.getColumnIndex(ROOMID)));
                mMap.put(ROOMNAME,cursor.getString(cursor.getColumnIndex(ROOMNAME)));
                mMap.put(SW1_ID,cursor.getString(cursor.getColumnIndex(SW1_ID)));
                mMap.put(SW1,cursor.getString(cursor.getColumnIndex(SW1)));
                mMap.put(SW2_ID,cursor.getString(cursor.getColumnIndex(SW2_ID)));
                mMap.put(SW2,cursor.getString(cursor.getColumnIndex(SW2)));
                mMap.put(SW3_ID,cursor.getString(cursor.getColumnIndex(SW3_ID)));
                mMap.put(SW3,cursor.getString(cursor.getColumnIndex(SW3)));
                mMap.put(SW4_ID,cursor.getString(cursor.getColumnIndex(SW4_ID)));
                mMap.put(SW4,cursor.getString(cursor.getColumnIndex(SW4)));
                mMap.put(SW5_ID,cursor.getString(cursor.getColumnIndex(SW5_ID)));
                mMap.put(SW5,cursor.getString(cursor.getColumnIndex(SW5)));

                mMap.put(SW6_ID,cursor.getString(cursor.getColumnIndex(SW6_ID)));
                mMap.put(SW6,cursor.getString(cursor.getColumnIndex(SW6)));

                mMap.put(SWITCHIP,cursor.getString(cursor.getColumnIndex(SWITCHIP)));
                mMap.put(ROOM_LOCATION,cursor.getString(cursor.getColumnIndex(ROOM_LOCATION)));
                mMap.put(TOPIC,cursor.getString(cursor.getColumnIndex(TOPIC)));
                mMap.put(SWITCHDETAILS,cursor.getString(cursor.getColumnIndex(SWITCHDETAILS)));
                mMap.put(PANELDEATILS,cursor.getString(cursor.getColumnIndex(PANELDEATILS)));
                mMap.put(IOTPRODSHORTCUT,cursor.getString(cursor.getColumnIndex(IOTPRODSHORTCUT)));
                mMap.put(CURTAINVALUE,cursor.getString(cursor.getColumnIndex(CURTAINVALUE)));
                mMap.put(IOTPRODNAME,cursor.getString(cursor.getColumnIndex(IOTPRODNAME)));
                mMap.put(LOCKVALUE,cursor.getString(cursor.getColumnIndex(LOCKVALUE)));
               // mMap.put(SWITCHLIST,cursor.getString(cursor.getColumnIndex(SWITCHLIST)));
                pannelList.add(mMap);

            } while (cursor.moveToNext());
        }

        return pannelList;
    }

    public void deleteHomeTable() {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_HOME, null, null);
        db.close();
    }

    public void deleteTowerTable() {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_TOWER, null, null);
        db.close();
    }


    public void addNewConfigFile(String stringPanel, String homeID, String panel, String roomName, String switchDeatails,String curtain_val,String iot_product,String mainlock) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HOMEID, homeID);
        values.put(TOPIC,panel);
        values.put(PANELDEATILS,stringPanel);
        values.put(ROOMNAME,roomName);
        values.put(SWITCHDETAILS,switchDeatails);
        values.put(CURTAINVALUE,curtain_val);
        values.put(IOTPRODSHORTCUT,iot_product);
        values.put(LOCKVALUE,mainlock);

        Cursor cursor = db.rawQuery("select * from "+TABLE_HOME+" where "+TOPIC+"='"+panel+"'",null);
        // Inserting Row
        try {
            if(cursor!=null && cursor.getCount()>0){
                long mStatus=db.update(TABLE_HOME, values, TOPIC + "=" +panel,null);
            }else {
                long s = db.insert(TABLE_HOME, null, values);
            }

        }catch(Exception e){
            Log.d("Switch Insert ","Fail");
        }
        //2nd argument is String containing nullColumnHack
        db.close();

    }

    public void deletePanel(String position) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HOME,KEY_ID+"=" +position,null);
        db.close();

    }

    public HashMap<String,String> getHomeInfo(String itemType) {

        //ArrayList<HashMap<String,String>> pannelList = new ArrayList<HashMap<String,String>>();
        // Select All Query

        HashMap<String,String> mMap=new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_HOME+" where "+KEY_ID+"="+ itemType;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                mMap.put(KEY_ID,cursor.getString(cursor.getColumnIndex(KEY_ID)));
                mMap.put(ROOMNAME,cursor.getString(cursor.getColumnIndex(ROOMNAME)));
                mMap.put(SW1_ID,cursor.getString(cursor.getColumnIndex(SW1_ID)));
                mMap.put(SW1,cursor.getString(cursor.getColumnIndex(SW1)));
                mMap.put(SW2_ID,cursor.getString(cursor.getColumnIndex(SW2_ID)));
                mMap.put(SW2,cursor.getString(cursor.getColumnIndex(SW2)));
                mMap.put(SW3_ID,cursor.getString(cursor.getColumnIndex(SW3_ID)));
                mMap.put(SW3,cursor.getString(cursor.getColumnIndex(SW3)));
                mMap.put(SW4_ID,cursor.getString(cursor.getColumnIndex(SW4_ID)));
                mMap.put(SW4,cursor.getString(cursor.getColumnIndex(SW4)));
                mMap.put(SW5_ID,cursor.getString(cursor.getColumnIndex(SW5_ID)));
                mMap.put(SW5,cursor.getString(cursor.getColumnIndex(SW5)));
                mMap.put(SW6_ID,cursor.getString(cursor.getColumnIndex(SW6_ID)));
                mMap.put(SW6,cursor.getString(cursor.getColumnIndex(SW6)));
                mMap.put(ROOM_LOCATION,cursor.getString(cursor.getColumnIndex(ROOM_LOCATION)));
                mMap.put(SWITCHDETAILS,cursor.getString(cursor.getColumnIndex(SWITCHDETAILS)));
                mMap.put(PANELDEATILS,cursor.getString(cursor.getColumnIndex(PANELDEATILS)));
                mMap.put(IOTPRODSHORTCUT,cursor.getString(cursor.getColumnIndex(IOTPRODSHORTCUT)));
                mMap.put(IOTPRODNAME,cursor.getString(cursor.getColumnIndex(IOTPRODNAME)));



            } while (cursor.moveToNext());
        }

        return mMap;


    }


    public String getGenieTower() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TOWER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String mTowerString="NA";

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            mTowerString="";
            do
            {
//                for(int i=0;i<cursor.getCount();i++) {
                    mTowerString = mTowerString + cursor.getString(cursor.getColumnIndex(TOWER_NAME));
                    Log.d("Tower Name",""+mTowerString);
                //}
            }
            while (cursor.moveToNext());
        }

        return mTowerString;
    }
}
