package gsmartadmin.genieiot.com.myapplication.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by root on 11/14/16.
 */

public class Utils {

    public static final String PREFS_NAME = "GSMART_PREFS";
    public static final String HOMEID = "HomeId";
    public static final String PREFS_KEY_SSID ="SSID";
    public static final String PREFS_KEY_SHAREDKEY = "PASSWORD";
    public static final String SECURE_SSID = "SecureSSID";
    public static final String SECURE_KEY = "SecureKey";
    public static final String HOME_FLAG="Home_Flag";

//    public static final String URL_GENIE = "http://192.168.0.119:8080/GSmart_final_9jan/";
//    public static String URL_GENIE_AWS = "http://103.205.140.65:8080/GSmart_geniecentraltest/"; //Raspberry

    public static final String URL_GENIE = "http://192.168.0.119:8080/smart_home_local";
    //public static final String URL_GENIE = "http://192.168.0.18:7070";
    public static String URL_GENIE_AWS = "http://gsmarthome.genieiot.in"; //Raspberry
    //public static String URL_GENIE_AWS = "http://192.168.0.18:6060"; //Raspberry
  //  public static String URL_GENIE_AWS = "http://192.168.0.2:6060/";


    // public static String URL_GENIE_AWS = "http://geniecentraltest.genieiot.com:8080/GSmart_geniecentraltest/"; //Raspberry
    // public static final String URL_GENIE_AWS = "http://preprod.genieiot.com:8080/GSmart_PreProd/";




    public static final MediaType JSON = MediaType.parse("application/json; charset=UTF8");
    OkHttpClient client = new OkHttpClient();


    public static void saveSharedP(Context context, String key,String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(key,text);
        editor.commit();
    }

    public static String getSharedPValue(Context context,String key) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(key,"");
        return text;
    }
    public static boolean isInternetAvailable(Context context) {

        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(24, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(24, TimeUnit.SECONDS);    //Socket timeout
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}