package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import gsmartadmin.genieiot.com.myapplication.Adapter.CustomSpinnerAdapter;
import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Hotspot.WifiAPController;
import gsmartadmin.genieiot.com.myapplication.Utils.PanelDetails;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

import static gsmartadmin.genieiot.com.myapplication.R.id.spSwitchType;
import static gsmartadmin.genieiot.com.myapplication.Utils.Utils.URL_GENIE_AWS;

/**
 * Created by root on 3/2/17.
 */

public class ReconfigActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    Spinner spSelectPanel;
    Button btnReConfiguration;
    DatabaseHandler handler;
    private ProgressDialog progressDialog;
    private static final String TAG = "ReconfigActivity";
    private SwitchSessionManager sessionManager;
    private Spinner spRoomType, spSwitchLocation, spIotProductType;
    private Button btnEnter, btnSubmit;
    private LinearLayout parentLayout, llDynamicSpinner, llHideLayout;
    private EditText edtSwitchNo, edtHomeID;
    private String mRoomName = "", mRoomLocation = "", mRoomID = "", mRoomLocationID = "", mRoomShortCut = "", mIOTProdName = "", mIOTProdID = "", mIOTProdShortCut = "";
    private String panelId = "",panelName = "",panelCommand = "";
    private String mFinalString , selectedPanelName;
    private ProgressDialog progress;
    private ArrayList<HashMap<String, String>> arrRoomList, arrSwitchList;
    private ArrayList<HashMap<String, String>> mMapResult;
    private WifiManager wifimanager;
    private ProgressDialog pDailog;
    private ArrayList<HashMap<String, String>> mSwitchListMap;
    private ArrayList<HashMap<String, String>> panelList;
    private ArrayList<PanelDetails> panelDetailsList;
    HashMap<String, String> mMap;
    SwitchSessionManager manager;
    String curtainValue = "";
    String lockValue = "";
    AppCompatCheckBox cbSwitchSelect;
    String mainLock = "";
    boolean flag = false;
    int count = 0;
    int panelNumber2;
    String response = null;
    Utils req = new Utils();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reconfig_activity);
        panelList = new ArrayList<>();
        arrRoomList = new ArrayList<>();
        arrSwitchList = new ArrayList<>();

        initControl();

        initRoomTypeSpinner();
        initSwitchLocationSpinner();
        initIOTProductTypeSpinner();

        new GetPanelList().execute();

        for (int i = 0; i < 6; i++) {
            AddView(i);
        }


    }

    private void initControl() {
        spSelectPanel = (Spinner) findViewById(R.id.spSelectPanel);
        parentLayout = (LinearLayout) findViewById(R.id.parentLayout);
        llHideLayout = (LinearLayout) findViewById(R.id.llHideLayout);
        llDynamicSpinner = (LinearLayout) findViewById(R.id.llDynamicSpinner);
        spRoomType = (Spinner) findViewById(R.id.spRoomType);
        spSwitchLocation = (Spinner) findViewById(R.id.spSwitchLocation);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edtSwitchNo = (EditText) findViewById(R.id.edtSwitchNo);
        edtHomeID = (EditText) findViewById(R.id.edtHomeID);
        spIotProductType = (Spinner) findViewById(R.id.spIotProductType);

        cbSwitchSelect = (AppCompatCheckBox) findViewById(R.id.cbSwitchSelect);

        edtHomeID.setText(Utils.getSharedPValue(this, Utils.HOMEID));

        handler = new DatabaseHandler(this);
        manager = new SwitchSessionManager(this);

        spRoomType.setOnItemSelectedListener(this);
        spSwitchLocation.setOnItemSelectedListener(this);
        spIotProductType.setOnItemSelectedListener(this);
        spSelectPanel.setOnItemSelectedListener(this);
        btnEnter.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        llDynamicSpinner.removeAllViews();

        cbSwitchSelect.setOnCheckedChangeListener(null);
        cbSwitchSelect.setChecked(false);


    }

    private void initRoomTypeSpinner() {

        ArrayList<HashMap<String, String>> arrRoomList = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.roomType);
        String[] switchArrID = getResources().getStringArray(R.array.roomType_id);
        String[] switchShortCut = getResources().getStringArray(R.array.roomShortCut);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            map.put("S_CUT", switchShortCut[i]);
            arrRoomList.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ReconfigActivity.this, arrRoomList);
        spRoomType.setAdapter(customSpinnerAdapter);

    }
    private void initRoomTypeSpinner(String roomName) {
        ArrayList<HashMap<String, String>> arrRoomList = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.roomType);
        String[] switchArrID = getResources().getStringArray(R.array.roomType_id);
        String[] switchShortCut = getResources().getStringArray(R.array.roomShortCut);

        String name = null;
        int pos = 0;
        for (int i = 0; i < switchArr.length; i++) {
            if (roomName.equalsIgnoreCase(switchShortCut[i])){
                name = switchArr[i];
                pos = i;
            }
        }

        spRoomType.setSelection(pos);
        Log.d("onItemSelected","name; "+name+" "+getIndex(spRoomType, name));

    }
    //private method of your class
    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }
        }

        return 0;
    }


    private void initSwitchLocationSpinner() {
        ArrayList<HashMap<String, String>> arrSwitchListLocation = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.switch_location_arr);
        String[] switchArrID = getResources().getStringArray(R.array.switch_location_id);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            arrSwitchListLocation.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ReconfigActivity.this, arrSwitchListLocation);
        spSwitchLocation.setAdapter(customSpinnerAdapter);

    }

    private void initIOTProductTypeSpinner() {

        ArrayList<HashMap<String, String>> arrSwitchProductType = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.iot_product_type);
        String[] switchArrID = getResources().getStringArray(R.array.iot_product_id);
        String[] switchShortCut = getResources().getStringArray(R.array.iot_prod_shortcut);
        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            map.put("I_Shortcut", switchShortCut[i]);
            arrSwitchProductType.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ReconfigActivity.this, arrSwitchProductType);
        spIotProductType.setAdapter(customSpinnerAdapter);
    }

    private void AddView(int curPos) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.row_layout, null);

        final Spinner spSwitchType = (Spinner) rowView.findViewById(R.id.spSwitchType);

        ArrayList<HashMap<String, String>> arrSwitchTypeList = new ArrayList<>();
        ArrayList<HashMap<String, String>> arrSwitchTypeListDimm = new ArrayList<>();


        String[] switchArr = getResources().getStringArray(R.array.switch_type);
        String[] switchArrID = getResources().getStringArray(R.array.switch_type_id);

        String[] switchArrDimm = getResources().getStringArray(R.array.switch_type_Dimm);
        String[] switchArrIDDimm = getResources().getStringArray(R.array.switch_type_id_Dimm);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            arrSwitchTypeListDimm.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(ReconfigActivity.this, arrSwitchTypeListDimm);
        spSwitchType.setAdapter(customSpinnerAdapter);
        llDynamicSpinner.addView(rowView, llDynamicSpinner.getChildCount() - 1);

    }
    @Override
    protected void onResume() {
        super.onResume();
        turnOnHotspot();
        curtainValue="NA";

    }

    @Override
    protected void onPause() {
        super.onPause();
        turnOffHotspot();
    }

    private void turnOnHotspot() {
        wifimanager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("App_Version", "Above Marshmellow");
            WifiAPController wifiAPController = new WifiAPController();
            wifiAPController.wifiToggle("geniesmart", "12341234", wifimanager, this);
        } else {
            Log.d("App_Version", "Below Marshmellow");
            setWifiApState(ReconfigActivity.this, true);
        }
    }

    private static final String SSID = "geniesmart";
    private static final String PASSWORD = "12341234";

    public static boolean setWifiApState(Context context, boolean enabled) {

        try {
            WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (enabled) {
                mWifiManager.setWifiEnabled(false);
            }

            WifiConfiguration conf = getWifiApConfiguration();
            mWifiManager.addNetwork(conf);

            return (Boolean) mWifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class).invoke(mWifiManager, conf, enabled);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public static WifiConfiguration getWifiApConfiguration() {
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = SSID;
        netConfig.preSharedKey = PASSWORD;
        netConfig.hiddenSSID = true;
        netConfig.status = WifiConfiguration.Status.ENABLED;
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        return netConfig;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                onClickOfSubmit();
                break;
              /*  case R.id.btnReConfiguration:
                    new AsyncGetConfigurationTask().execute();
                    break;*/
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spRoomType) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mRoomName = mMap.get("NAME");
            mRoomID = mMap.get("ID");
            mRoomShortCut = mMap.get("S_CUT");

            Log.d("onItemSelected","mRoomName: "+mRoomName);

        } else if (spinner.getId() == R.id.spSwitchLocation) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mRoomLocationID = mMap.get("ID");
            mRoomLocation = mMap.get("NAME");
        } else if (spinner.getId() == R.id.spIotProductType) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mIOTProdName = mMap.get("NAME");
            mIOTProdID = mMap.get("ID");
            mIOTProdShortCut = mMap.get("I_Shortcut");
            if (mIOTProdID.equals("2")) {
                llHideLayout.setVisibility(View.GONE);
            } else {
                llHideLayout.setVisibility(View.VISIBLE);
            }
        }else if (spinner.getId() == R.id.spSelectPanel) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            panelId = mMap.get("ID");
            panelName = mMap.get("NAME");
            panelCommand = mMap.get("panelCommand");

            panelNumber2 = Integer.parseInt(panelId);
            selectedPanelName = panelName;

            if (!panelCommand.isEmpty()) {
                String roomName = panelCommand.split("-")[1];
                initRoomTypeSpinner(roomName.trim());
            }
            Log.d("onItemSelected","panelName: "+panelName+" panelId: "+panelId);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void onClickOfSubmit() {

        //String selectedPanelName = spSelectPanel.getSelectedItem().toString();

        mSwitchListMap = new ArrayList<>();
        ArrayList<Integer> curtainIndex = new ArrayList<>();
        String mLockString = "";
        String mLockStatus = "NA";

       if (llDynamicSpinner.getChildCount() > 0) {
            String mSwitchString = "";
            for (int i = 0; i < llDynamicSpinner.getChildCount(); i++) {

                View lChild = llDynamicSpinner.getChildAt(i).findViewById(spSwitchType);
                Spinner sp = (Spinner) lChild;
                int spCount = sp.getSelectedItemPosition();
                Log.d("Value_of_spinner", "Item position=" + spCount);

                mMap = (HashMap<String, String>) sp.getItemAtPosition(spCount);
                Log.d("Value_of_spinner", "map value=" + mMap);
                mSwitchListMap.add(mMap);

                if (mMap.get("ID").equals("0")) {

                    mSwitchString = mSwitchString + ("0");
                    Log.d("Value_of_spinner", "0 mSwitchString=" + mSwitchString);
                } else if (mMap.get("NAME").equals("Curtains")) {

                    mSwitchString = mSwitchString + (i + 1);
                    Log.d("Value_of_spinner", "curtain mSwitchString=" + mSwitchString);
                    curtainIndex.add(i);

                } else {
                    mSwitchString = mSwitchString + (i + 1);
                    Log.d("Value_of_spinner", "i+1 mSwitchString=" + mSwitchString);
                }


                if (mMap.get("NAME").equals("Lock")) {
                    mLockString = mLockString + "L";
                    Log.d("Value_of_spinner", "name:lock mLockString=" + mLockString);
                } else {
                    mLockString = mLockString + "0";
                    Log.d("Value_of_spinner", "mLockString=" + mLockString);
                }

            }

            if (mLockString.contains("L")) {
                mLockStatus = mLockString.substring(0, 4);
                Log.d("Value_of_spinner", "mLockStatus=" + mLockStatus);
            }
            Log.d("Lock Value", mLockStatus);

            if (cbSwitchSelect.isChecked()) {
                mainLock = "1";
                if (mLockStatus.equals("NA")) {
                    Toast.makeText(this, "Select lock while selecting Mainlock", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {

                mainLock = "NA";
            }

            Log.d("Main Lock", "" + mainLock);

            if (curtainIndex.size() >= 2) {
                if (curtainIndex.size() < 3) {
                    int i = curtainIndex.get(curtainIndex.size() - 1);
                    curtainValue = "c" + i;
                    Log.d("Value_of_spinner", "curtainValue" + curtainValue);
                } else {
                    curtainValue = "cc";
                    Log.d("Value_of_spinner", "curtainValue" + curtainValue);
                }

            } else if (curtainIndex.size() == 0) {
                curtainValue = "NA";
            } else {
                Toast.makeText(this, "Select Two curtains...", Toast.LENGTH_SHORT).show();
            }
            Log.d("Curtain", " " + curtainValue);

            String strHomeId = edtHomeID.getText().toString().trim();

            if (strHomeId.equals("")) {
                Toast.makeText(this, "Please enter Home ID.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!curtainValue.equals("NA") && !mLockStatus.equals("NA")) {
                Toast.makeText(this, "Either select lock or curtain", Toast.LENGTH_SHORT).show();
                return;
            }
           if (selectedPanelName.equalsIgnoreCase("Select Panel")){
               Toast.makeText(getApplicationContext(),"Please select panel",Toast.LENGTH_SHORT).show();
               return;
           }

            if (curtainValue.equals("NA") && !mLockStatus.equals("NA")) {
                curtainValue = mLockStatus;
                Log.d("All Values", "curtainValue" + curtainValue);
            }

            if (mIOTProdID.equals("1")) {
                curtainValue = "gc";
            }

            Utils.saveSharedP(this, Utils.HOMEID, edtHomeID.getText().toString());
            mFinalString = mSwitchString;
            Log.d(TAG, "onClickOfSubmit: " + mFinalString + "-" + mIOTProdShortCut);

            getClientList(false);

        } else {
            Toast.makeText(this, "Please enter number of switches", Toast.LENGTH_SHORT).show();
        }
    }
    public ArrayList<HashMap<String, String>> getClientList(boolean onlyReachables) {
        return getClientList(onlyReachables, 300);
    }

    public ArrayList<HashMap<String, String>> getClientList(boolean onlyReachables, int reachableTimeout) {
        BufferedReader br = null;

        try {
            mMapResult = new ArrayList<>();
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                Log.d("line_value", "=" + line);
                String[] splitted = line.split(" +");

                if ((splitted != null) && (splitted.length >= 4)) {
                    // Basic sanity check
                    Log.d("splitted", "=" + splitted[0]);
                    String mac = splitted[3];

                    Log.d("mac", "mac" + mac);
                    if (mac.matches("..:..:..:..:..:..")) {//"c0:c9:76:72:fe:cf"

                        new GetConnectedUser(splitted, onlyReachables).execute();

                    } else {
                        //  Toast.makeText(MainActivity.this, "Device not connected to Hotspot.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }
        }

        return mMapResult;
    }

    class GetConnectedUser extends AsyncTask<Void, Void, Void> {
        String[] splitted;
        boolean onlyReachables;
        String CurrIP;
        private BufferedReader inFromServer;
        private String mFinalResult;

        public GetConnectedUser(String[] splitted, boolean onlyReachables) {
            this.splitted = splitted;
            this.onlyReachables = onlyReachables;
            CurrIP = splitted[0];
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDailog = new ProgressDialog(ReconfigActivity.this);
            pDailog.setMessage("Please wait...");
            pDailog.setCancelable(true);
            pDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Log.d("testing_value", "ipaddress== " + splitted[0]);
                boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(300);
                Log.d("testing_value", "reachable==" + isReachable);

                if (!onlyReachables || isReachable) {
                    HashMap<String, String> mMap = new HashMap<>();
                    mMap.put("1", splitted[0]);
                    Log.d("splitted_value", "==" + splitted[0]);
                    mMap.put("2", splitted[3]);
                    Log.d("splitted_value", "==" + splitted[3]);
                    mMap.put("3", splitted[5]);
                    Log.d("splitted_value", "==" + splitted[5]);
                    mMapResult.add(mMap);
                }

                if (splitted[0] != null && !splitted[0].equals("")) {

                    Socket pingSocket = null;
                    PrintWriter out = null;

                    BufferedReader in = null;
                    Log.d("testing_value", "inside ");
                    try {
                        Log.d("splitted_value", "==" + splitted[0]);
                        pingSocket = new Socket(splitted[0], 23);

                        out = new PrintWriter(pingSocket.getOutputStream(), true);      //for sending daata //write  //printwriter for text format

                        Log.d("testing_value", "print value======= " + out.toString());
                        Log.d("testing_value", "printwriter value== " + splitted[0]);


                        Log.d("testing_value", "socket value== " + pingSocket.getOutputStream().toString());

                    } catch (final IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (pDailog != null) {
                                    pDailog.cancel();
                                }

                                Toast.makeText(ReconfigActivity.this, "Device not connected.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    if (out != null) {

                        SwitchSessionManager session = new SwitchSessionManager(ReconfigActivity.this);
                        String sid = ":s" + session.getPSSID() + "," + session.getPPASSWORD() + ",/r/n";
                        String pid = ":p" + session.getSSSID() + "," + session.getSPASSWORD() + ",/r/n";

                        out.println(sid);


                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        out.println(pid);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        Log.d("SEND_CONFIG", "panel nmber in send socket==" + selectedPanelName);
                        int panelNumber = panelNumber2;

                        final String switchDeatils = mRoomShortCut + "-" + panelNumber + "-" + mFinalString + "-" + Utils.getSharedPValue(ReconfigActivity.this, Utils.HOMEID);
                        Log.d("Panel_Number", "ALL SWITCH===" + switchDeatils);

                        final String mSwitchInfo = ":c" + switchDeatils + ",/r/n";
                        out.println(mSwitchInfo);

                        Log.d("Switch Info", mSwitchInfo);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                       // final String panel = "panel" + panelNumberS;
                        final String panel = selectedPanelName;

                        final String panelDetails = panel + "," + panel + "/in," + panel + "/out," + session.getTopicName() + ",";

                        final String checkPanelDetails = panel + "-" + panel + "/in-" + panel + "/out-" + session.getTopicName();

                        String mPanelInfo = ":t" + panelDetails + "/r/n";
                        out.println(mPanelInfo);
                        Log.d("Panel Info", mPanelInfo);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        String mAllInfo = ":f";
                        out.println(mAllInfo);

                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                        }

                        byte[] buffer = new byte[2048];
                        int bytes;
                        InputStream ing = pingSocket.getInputStream();
                        bytes = ing.read(buffer);
                        String readMessage = new String(buffer, 0, bytes);

                        Log.d("MainActivity->", "Message :: " + readMessage);

                        String CurrString = switchDeatils + "-" + session.getPSSID() + "-" + session.getPPASSWORD() + "-" + session.getSSSID() + "-" + session.getSPASSWORD() + "-" + checkPanelDetails;
                        Log.d("Current String", CurrString);

                        if (readMessage != null) {
                            readMessage = readMessage.replaceAll("\\r\\n", "");
                        }

                        Log.d(":F String ", readMessage);

                        if (!CurrString.equals(readMessage)) {
                            System.out.print("Do it again.");
                            out.close();
                            pingSocket.close();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ReconfigActivity.this, "Missmatch the data.Do it again.", Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            Log.d("Successs", "Write Successfully");
                            out.close();
                            pingSocket.close();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pDailog != null && pDailog.isShowing()) {
                                        pDailog.cancel();
                                    }

                                    if (mIOTProdID.equals("2")) {
                                        DatabaseHandler db = new DatabaseHandler(ReconfigActivity.this);
                                        db.addGenieTower();
                                        llHideLayout.setVisibility(View.VISIBLE);
                                        initIOTProductTypeSpinner();
                                        Toast.makeText(ReconfigActivity.this, "Genie tower added successfully", Toast.LENGTH_LONG).show();
                                    } else {
                                        HashMap<String, String> mapInfo = new HashMap<>();
                                        mapInfo.put(DatabaseHandler.ROOMID, mRoomID);
                                        mapInfo.put(DatabaseHandler.ROOMNAME, mRoomName);
                                        mapInfo.put(DatabaseHandler.ROOM_LOCATION, mRoomLocation);
                                        mapInfo.put(DatabaseHandler.SWITCHIP, splitted[0]);
                                        mapInfo.put(DatabaseHandler.HOMEID, Utils.getSharedPValue(ReconfigActivity.this, Utils.HOMEID));
                                        mapInfo.put(DatabaseHandler.TOPIC, panel);
                                        mapInfo.put(DatabaseHandler.SWITCHDETAILS, switchDeatils);
                                        Log.d("switchdtails_values", "===" + switchDeatils);
                                        mapInfo.put(DatabaseHandler.PANELDEATILS, panelDetails);
                                        mapInfo.put(DatabaseHandler.IOTPRODSHORTCUT, mIOTProdShortCut);
                                        mapInfo.put(DatabaseHandler.CURTAINVALUE, curtainValue);
                                        mapInfo.put(DatabaseHandler.IOTPRODNAME, mIOTProdName);
                                        mapInfo.put(DatabaseHandler.LOCKVALUE, mainLock);
                                        DatabaseHandler db = new DatabaseHandler(ReconfigActivity.this);

                                        Log.d(TAG, "run: " + mapInfo);
                                        db.addHomeInfo(mSwitchListMap, mapInfo);
                                        Toast.makeText(ReconfigActivity.this, "Data Send Succssfully", Toast.LENGTH_SHORT).show();

                                    }

                                    if (mainLock.equals("1")) {
                                        manager.setMAINLOCK(mainLock);
                                    }

                                    initSwitchLocationSpinner();
                                    initRoomTypeSpinner();
                                    llDynamicSpinner.removeAllViews();
                                    for (int i = 0; i < 6; i++) {
                                        AddView(i);
                                    }


                                    Intent intent = new Intent(ReconfigActivity.this, PauseHotspot.class);
                                    startActivity(intent);


                                }
                            });
                        }

                    }

                }


            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (pDailog != null && pDailog.isShowing()) {
                            pDailog.cancel();
                        }

                        Toast.makeText(ReconfigActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDailog != null && pDailog.isShowing()) {
                pDailog.cancel();
            }

            //Toast.makeText(MainActivity.this,"Please try again.",Toast.LENGTH_SHORT).show();
        }
    }
    public void turnOffHotspot() {

        wifimanager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);

        try {
            Method[] wmMethods = wifimanager.getClass().getDeclaredMethods();
            WifiConfiguration wifiConfiguration = new WifiConfiguration();

            for (Method method : wmMethods) {
                if (method.getName().equals("setWifiApEnabled")) {
                    try {
                        method.invoke(this.wifimanager, wifiConfiguration, false);

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            Log.d("Hotspot", "Hotspot turn off");
        }

    }
    private class GetPanelList extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String response = null;
            try {
                JSONObject jObj = new JSONObject();
                jObj.put("homeId", manager.getHOMEID());

                response = req.doPostRequest(URL_GENIE_AWS + "/home/getPanelListByHome", "" + jObj);
                Log.d("SEND_CONFIG", "URL: "+URL_GENIE_AWS + "/home/getPanelListByHome"+" Response===" + response);

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("SEND_CONFIG", "error===" + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("SEND_CONFIG", "error====" + e.getMessage());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            try {
                panelList = new ArrayList<>();
                panelDetailsList = new ArrayList<>();

                HashMap<String, String> map1 = new HashMap<>();
                map1.put("ID", "0");
                map1.put("panelIp", "");
                map1.put("panelCommand", "");
                map1.put("notImplimented", "");
                map1.put("NAME", "Select Panel");
                panelList.add(map1);

                JSONArray jResult = new JSONArray(response);
                Log.d("SEND_CONFIG","jResult: "+jResult.length());
                for (int i =0; i<jResult.length();i++){
                    JSONObject object = jResult.getJSONObject(i);
                    String panelId = object.getString("panelId");
                    String panelIp = object.getString("panelIp");
                    String panelCommand = object.getString("panelCommand");
                    String notImplimented = object.getString("notImplimented");
                    String panelName = object.getString("panelName");

                    HashMap<String, String> map = new HashMap<>();
                    map.put("ID", panelId);
                    map.put("panelIp", panelIp);
                    map.put("panelCommand", panelCommand);
                    map.put("notImplimented", notImplimented);
                    map.put("NAME", panelName);
                    panelList.add(map);
                    panelDetailsList.add(new PanelDetails(panelId,panelIp,panelCommand,notImplimented,panelName));

                }
                Log.d("SEND_CONFIG","panelList size: "+panelList.size());
                Log.d("SEND_CONFIG","panelDetailsList size: "+panelDetailsList.size());

                CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(ReconfigActivity.this, panelList);
                spSelectPanel.setAdapter(customSpinnerAdapter1);


            }catch (Exception e){
                e.printStackTrace();
                Log.d("SEND_CONFIG", "error===" + e.getMessage());
            }

        }
    }
    public class AsyncGetConfigurationTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ReconfigActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response = null;
            Utils req = new Utils();
            Object panelNumber;
            try {


                response = req.doGetRequest("http://" + sessionManager.getTopicName() + ":8080/smart_home_local/home/getinstallationinfo");
               /* response=req.doGetRequest("http://192.168.0.2:6060/home/getLastIotProductNumber");
                Log.d("Get_All_Data ","URL==" +"http://192.168.0.2:6060/home/getLastIotProductNumber==="+response);

                JSONObject jResult=new JSONObject(response);
                panelNumber= jResult.get("last_iot_product_number");
                Log.d("Get_All_Data","last value==="+panelNumber.toString());*/

            } catch (Exception e) {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.cancel();
            }

            if (result != null) {
//                String mm="{\"panelDetails\":\"panel1-BED1-11-123456-GTX11,panel2-BED2-12-120056-GTX11,panel3-HAL1-13-123456-GTX11\",\n" +
//                        "\"status\":\"SUCCESS\"\n" +
//                        "}";
                parseJSONResponse(result);
            } else {
                Toast.makeText(ReconfigActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void parseJSONResponse(String result) {
        try {
            JSONObject jResult = new JSONObject(result);

            if (jResult.getString("status").equals("SUCCESS")) {
                String panelDetails = jResult.getString("panelDetails");
                String[] panelinfo = panelDetails.split(",");
//             String routeSSID=jResult.getString("routerSSID");
//             String routerPass=jResult.getString("routerPassword");
//             String secureSSID=jResult.getString("secureSSID");
//             String securePassword=jResult.getString("securePassword");

                String homeId = Utils.getSharedPValue(ReconfigActivity.this, Utils.HOMEID);
                for (int i = 0; i < panelDetails.length(); i++) {
                    String[] splitpanel = panelinfo[i].split("-");
                    String topic = splitpanel[0];
                    String roomName = splitpanel[1];
                    String switchDetails = splitpanel[1] + "-" + splitpanel[2] + "-" + splitpanel[3] + "-" + splitpanel[4];
                    handler.addNewConfigFile(panelinfo[i], homeId, topic, roomName, switchDetails, splitpanel[5], splitpanel[6], splitpanel[7]);
                }
                finish();
                //Toast.makeText(this,"Data Fetch successfully.",Toast.LENGTH_LONG).show();
                Toast.makeText(this, "Please Check Room List", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Unable to fetch configure file.", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
