package gsmartadmin.genieiot.com.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.skyfishjy.library.RippleBackground;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import gsmartadmin.genieiot.com.myapplication.Hotspot.WifiAPController;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

import static gsmartadmin.genieiot.com.myapplication.R.id.centerImage;

/**
 * Created by root on 11/11/16.
 */

public class HotspotActivity extends AppCompatActivity implements View.OnClickListener{
    private AVLoadingIndicatorView avi;
    WifiManager wifimanager;
    private Button btnTurnOff;
    RippleBackground rippleBackground;
    private ImageView imageView;
    ArrayList<HashMap<String,String>> result = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hotspot_activity);

        btnTurnOff= (Button) findViewById(R.id.btnTurnOff);
        btnTurnOff.setOnClickListener(this);


        rippleBackground=(RippleBackground)findViewById(R.id.content);
        imageView=(ImageView)findViewById(centerImage);
        imageView.setOnClickListener(this);
        rippleBackground.startRippleAnimation();


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
               //Toast.makeText(HotspotActivity.this,"Hotspot TurnOff successfully.",Toast.LENGTH_SHORT).show();
               Intent intent=new Intent(HotspotActivity.this,MainActivity.class);
               startActivity(intent);
            }

        }, 4000);


        //wifimanager = (WifiManager) this.getSystemService(WIFI_SERVICE);
        wifimanager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);
        Method[] methods = wifimanager.getClass().getDeclaredMethods();
        for (Method m: methods) {
            if (m.getName().equals("getWifiApConfiguration")) {
                try {
                    WifiConfiguration config = (WifiConfiguration)m.invoke(wifimanager);
                    if(config.SSID.toString()!=null) {
                        Utils.saveSharedP(this, Utils.PREFS_KEY_SSID ,config.SSID.toString());
                        Utils.saveSharedP(this, Utils.PREFS_KEY_SHAREDKEY ,config.preSharedKey.toString());
                        Toast.makeText(this,Utils.getSharedPValue(this,Utils.PREFS_KEY_SSID)+" "+  config.preSharedKey.toString(),Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this,"Not getting ssid.", Toast.LENGTH_SHORT).show();
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                // here, the "config" variable holds the info, your SSID is in
                // config.SSID
            }
        }

        //wifimanager.setWifiEnabled(false);
        WifiAPController wifiAPController  = new WifiAPController();
        wifiAPController.wifiToggle("openaccess2", "12345678", wifimanager, this);
    }



    public void setLocationSettingOnOff(){

        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Services Not Active");
            builder.setMessage("Please enable Location Services and GPS");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setLocationSettingOnOff();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // Toast.makeText(this, "onDestroy()", Toast.LENGTH_SHORT).show();
       // WifiAPController wifiAPController  = new WifiAPController();
       // wifiAPController.wifiToggle("Hari","", wifimanager, this);
    }

    public void turnOffHotspot(){
        try
        {
            Method[] wmMethods = wifimanager.getClass().getDeclaredMethods();
            WifiConfiguration wifiConfiguration = new WifiConfiguration();

            for (Method method : wmMethods) {
                if (method.getName().equals("setWifiApEnabled")) {
                    try {

                        method.invoke(this.wifimanager, wifiConfiguration, false);

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e)
        {
            Log.d("Hotspot", "Hotspot turn off");

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnTurnOff:
                rippleBackground.stopRippleAnimation();

//                turnOffHotspot();
//                Intent intent=new Intent(HotspotActivity.this,MainActivity.class);
//               startActivity(intent);
               // setSSID();
              //  turnOffHotspot();

              //  final Handler handler = new Handler();
              //  handler.postDelayed(new Runnable() {
               //     @Override
                //    public void run() {
                        // Do something after 5s = 5000ms
                       // WifiAPController wifiAPController  = new WifiAPController();
                       // wifiAPController.wifiToggle("Hari","12345678", wifimanager, HotspotActivity.this);
               //     }
        //        }, 6000);



 //               ArrayList<HashMap<String,String>> clients = getClientList(false);



                break;
            case R.id.centerImage:

                break;
        }
    }

    public ArrayList<HashMap<String,String>> getClientList(boolean onlyReachables) {
        return getClientList(onlyReachables, 300);
    }


    public ArrayList<HashMap<String,String>> getClientList(boolean onlyReachables, int reachableTimeout) {
        BufferedReader br = null;

        try {
            result = new ArrayList<>();
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");

                if ((splitted != null) && (splitted.length >= 4)) {
                    // Basic sanity check
                    String mac = splitted[3];

                    if (mac.matches("..:..:..:..:..:..")) {//"c0:c9:76:72:fe:cf"

                        new GetConnectedUser(splitted,onlyReachables).execute();

                    }
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }
        }

        return result;
    }

    class  GetConnectedUser extends AsyncTask<Void,Void,Void>{
        String[] splitted;
        boolean onlyReachables;
        public GetConnectedUser(String[] splitted,boolean onlyReachables)
        {
            this.splitted=splitted;
            this.onlyReachables=onlyReachables;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(300);

                if (!onlyReachables || isReachable) {
                    HashMap<String,String> mMap=new HashMap<>();
                    mMap.put("1",splitted[0]);
                    mMap.put("2",splitted[3]);
                    mMap.put("3",splitted[5]);
                    result.add(mMap);
                }

                if(splitted[0]!=null && !splitted[0].equals(""))
                {

                    Socket pingSocket = null;
                    PrintWriter out = null;
                    BufferedReader in = null;

                    try {
                        pingSocket = new Socket(splitted[0],23);
                        out = new PrintWriter(pingSocket.getOutputStream(), true);


                    } catch (final IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HotspotActivity.this, "Check hotspot", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    if(out!=null) {

                        BufferedReader inFromServer;
                        inFromServer = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
                        String strRead=inFromServer.readLine();
                        Log.d("Response",strRead);

                        out.println(":sgeniesmart,12341234,");
                        out.close();
                        pingSocket.close();

                    }

                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

   // WifiConfiguration wifiConfiguration = new WifiConfiguration();
   // initWifiAPConfig(wifiConfiguration);

    public void setSSID(){

       // WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = "\""+Utils.getSharedPValue(this,Utils.PREFS_KEY_SSID)+"\"";
        wc.preSharedKey  = "\""+Utils.getSharedPValue(this,Utils.PREFS_KEY_SHAREDKEY)+"\"";
        wc.hiddenSSID = true;
        wc.status = WifiConfiguration.Status.ENABLED;
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        int res = wifi.addNetwork(wc);
        Log.d("WifiPreference", "add Network returned " + res );
        boolean b = wifi.enableNetwork(res, true);
        Log.d("WifiPreference", "enableNetwork returned " + b );
        wifi.saveConfiguration();

    }

}
