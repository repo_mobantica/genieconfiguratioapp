package gsmartadmin.genieiot.com.myapplication.Utils;


public class PanelDetails {

    private String panelId;
    private String panelIp;
    private String panelCommand;
    private String notImplimented;
    private String panelName;

    public PanelDetails(String panelId, String panelIp, String panelCommand, String notImplimented, String panelName) {
        this.panelId = panelId;
        this.panelIp = panelIp;
        this.panelCommand = panelCommand;
        this.notImplimented = notImplimented;
        this.panelName = panelName;
    }

    public String getPanelId() {
        return panelId;
    }

    public void setPanelId(String panelId) {
        this.panelId = panelId;
    }

    public String getPanelIp() {
        return panelIp;
    }

    public void setPanelIp(String panelIp) {
        this.panelIp = panelIp;
    }

    public String getPanelCommand() {
        return panelCommand;
    }

    public void setPanelCommand(String panelCommand) {
        this.panelCommand = panelCommand;
    }

    public String getNotImplimented() {
        return notImplimented;
    }

    public void setNotImplimented(String notImplimented) {
        this.notImplimented = notImplimented;
    }

    public String getPanelName() {
        return panelName;
    }

    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }
}
