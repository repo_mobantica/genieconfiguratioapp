package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import gsmartadmin.genieiot.com.myapplication.Adapter.CustomSpinnerAdapter;
import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Hotspot.WifiAPController;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

import static gsmartadmin.genieiot.com.myapplication.R.id.spSwitchType;
import static gsmartadmin.genieiot.com.myapplication.Utils.Utils.URL_GENIE_AWS;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private Spinner spRoomType, spSwitchLocation, spIotProductType;
    private Button btnEnter, btnSubmit;
    private LinearLayout parentLayout, llDynamicSpinner, llHideLayout;
    private EditText edtSwitchNo, edtHomeID;
    private String mRoomName = "", mRoomLocation = "", mRoomID = "", mRoomLocationID = "", mRoomShortCut = "", mIOTProdName = "", mIOTProdID = "", mIOTProdShortCut = "";
    private String mFinalString;
    private ProgressDialog progress;
    private ArrayList<HashMap<String, String>> arrRoomList, arrSwitchList;
    private ArrayList<HashMap<String, String>> mMapResult;
    private WifiManager wifimanager;
    private ProgressDialog pDailog;
    private ArrayList<HashMap<String, String>> mSwitchListMap;
    private ArrayList<HashMap<String, String>> panelList;
    private ProgressDialog progressDialog;
    HashMap<String, String> mMap;
    SwitchSessionManager manager;
    DatabaseHandler handler;
    String curtainValue = "";
    String lockValue = "";
    AppCompatCheckBox cbSwitchSelect;
    String mainLock = "";
    boolean flag = false;
    private static final String TAG = "MainActivity";
    int count = 0;
    int panelNumber2, panelNumber3, total_room_size;
    int last_Switch_Id,last_Switch_Id1 ;
    String str_switchList = "";
    String response = null;
    Utils req = new Utils();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        panelList = new ArrayList<>();
        arrRoomList = new ArrayList<>();
        arrSwitchList = new ArrayList<>();

        initControl();

        initRoomTypeSpinner();
        initSwitchLocationSpinner();
        initIOTProductTypeSpinner();


        new Lastproductid().execute();


        for (int i = 0; i < 6; i++) {
            AddView(i);
        }


    }

    private void initIOTProductTypeSpinner() {

        ArrayList<HashMap<String, String>> arrSwitchProductType = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.iot_product_type);
        String[] switchArrID = getResources().getStringArray(R.array.iot_product_id);
        String[] switchShortCut = getResources().getStringArray(R.array.iot_prod_shortcut);
        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            map.put("I_Shortcut", switchShortCut[i]);
            arrSwitchProductType.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MainActivity.this, arrSwitchProductType);
        spIotProductType.setAdapter(customSpinnerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        turnOnHotspot();
        curtainValue = "NA";

//        if(manager.getMAINLOCK().equals("1")) {
//            cbSwitchSelect.setEnabled(false);
//        }
//        cbSwitchSelect.setChecked(false);

    }

    private void turnOnHotspot() {
        wifimanager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("App_Version", "Above Marshmellow");
            WifiAPController wifiAPController = new WifiAPController();
            wifiAPController.wifiToggle("geniesmart", "12341234", wifimanager, this);
        } else {
            Log.d("App_Version", "Below Marshmellow");
            setWifiApState(MainActivity.this, true);
        }
    }

    private static final String SSID = "geniesmart";
    private static final String PASSWORD = "12341234";

    public static boolean setWifiApState(Context context, boolean enabled) {

        try {
            WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (enabled) {
                mWifiManager.setWifiEnabled(false);
            }

            WifiConfiguration conf = getWifiApConfiguration();
            mWifiManager.addNetwork(conf);

            return (Boolean) mWifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class).invoke(mWifiManager, conf, enabled);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static WifiConfiguration getWifiApConfiguration() {
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = SSID;
        netConfig.preSharedKey = PASSWORD;
        netConfig.hiddenSSID = true;
        netConfig.status = WifiConfiguration.Status.ENABLED;
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        return netConfig;
    }

    private void initSwitchLocationSpinner() {
        ArrayList<HashMap<String, String>> arrSwitchListLocation = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.switch_location_arr);
        String[] switchArrID = getResources().getStringArray(R.array.switch_location_id);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            arrSwitchListLocation.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MainActivity.this, arrSwitchListLocation);
        spSwitchLocation.setAdapter(customSpinnerAdapter);

    }

    private void initControl() {
        parentLayout = (LinearLayout) findViewById(R.id.parentLayout);
        llHideLayout = (LinearLayout) findViewById(R.id.llHideLayout);
        llDynamicSpinner = (LinearLayout) findViewById(R.id.llDynamicSpinner);
        spRoomType = (Spinner) findViewById(R.id.spRoomType);
        spSwitchLocation = (Spinner) findViewById(R.id.spSwitchLocation);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edtSwitchNo = (EditText) findViewById(R.id.edtSwitchNo);
        edtHomeID = (EditText) findViewById(R.id.edtHomeID);
        spIotProductType = (Spinner) findViewById(R.id.spIotProductType);

        cbSwitchSelect = (AppCompatCheckBox) findViewById(R.id.cbSwitchSelect);

        edtHomeID.setText(Utils.getSharedPValue(this, Utils.HOMEID));

        handler = new DatabaseHandler(MainActivity.this);
        manager = new SwitchSessionManager(this);

        spRoomType.setOnItemSelectedListener(this);
        spSwitchLocation.setOnItemSelectedListener(this);
        spIotProductType.setOnItemSelectedListener(this);
        btnEnter.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        llDynamicSpinner.removeAllViews();

        cbSwitchSelect.setOnCheckedChangeListener(null);
        cbSwitchSelect.setChecked(false);


    }

    private void initRoomTypeSpinner() {

        ArrayList<HashMap<String, String>> arrRoomList = new ArrayList<>();
        String[] switchArr = getResources().getStringArray(R.array.roomType);
        String[] switchArrID = getResources().getStringArray(R.array.roomType_id);
        String[] switchShortCut = getResources().getStringArray(R.array.roomShortCut);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            map.put("S_CUT", switchShortCut[i]);
            arrRoomList.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MainActivity.this, arrRoomList);
        spRoomType.setAdapter(customSpinnerAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spRoomType) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mRoomName = mMap.get("NAME");
            mRoomID = mMap.get("ID");
            mRoomShortCut = mMap.get("S_CUT");

        } else if (spinner.getId() == R.id.spSwitchLocation) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mRoomLocationID = mMap.get("ID");
            mRoomLocation = mMap.get("NAME");
        } else if (spinner.getId() == R.id.spIotProductType) {
            HashMap<String, String> mMap = (HashMap<String, String>) parent.getItemAtPosition(position);
            mIOTProdName = mMap.get("NAME");
            mIOTProdID = mMap.get("ID");
            mIOTProdShortCut = mMap.get("I_Shortcut");
            if (mIOTProdID.equals("2")) {
                llHideLayout.setVisibility(View.GONE);
            } else {
                llHideLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnter:
                //onClickOfEnter();
                break;
            case R.id.btnSubmit:
                onClickOfSubmit();
                break;

        }
    }

    private void onClickOfSubmit() {

        mSwitchListMap = new ArrayList<>();
        ArrayList<Integer> curtainIndex = new ArrayList<>();
        String mLockString = "";
        String mLockStatus = "NA";
        str_switchList = "";

        if (llDynamicSpinner.getChildCount() > 0) {
            String mSwitchString = "";
            for (int i = 0; i < llDynamicSpinner.getChildCount(); i++) {

                View lChild = llDynamicSpinner.getChildAt(i).findViewById(spSwitchType);
                Spinner sp = (Spinner) lChild;
                int spCount = sp.getSelectedItemPosition();
                Log.d("Value_of_spinner", "Item position=" + spCount);

                mMap = (HashMap<String, String>) sp.getItemAtPosition(spCount);
                Log.d("Value_of_spinner", "map value=" + mMap);
                mSwitchListMap.add(mMap);

                if (mMap.get("ID").equals("0")) {

                    mSwitchString = mSwitchString + ("0");
                    Log.d("Value_of_spinner", "0 mSwitchString=" + mSwitchString);
                } else if (mMap.get("NAME").equals("Curtains")) {

                    mSwitchString = mSwitchString + (i + 1);
                    if (str_switchList.isEmpty()) {
                        str_switchList = str_switchList+ (last_Switch_Id1 + 1);
                    }else {
                        str_switchList = str_switchList+","+(last_Switch_Id1 + 1);
                    }
                    last_Switch_Id1 = last_Switch_Id1 + 1;
                    Log.d("Value_of_spinner", "curtain mSwitchString=" + mSwitchString);
                    curtainIndex.add(i);

                } else {
                    mSwitchString = mSwitchString + (i + 1);
                    if (str_switchList.isEmpty()) {
                        str_switchList = str_switchList+ (last_Switch_Id1 + 1);
                    }else {
                        str_switchList = str_switchList+","+(last_Switch_Id1 + 1);
                    }
                    last_Switch_Id1 = last_Switch_Id1 + 1;
                    Log.d("Value_of_spinner", "i+1 mSwitchString=" + mSwitchString);
                }


                if (mMap.get("NAME").equals("Lock")) {
                    mLockString = mLockString + "L";
                    Log.d("Value_of_spinner", "name:lock mLockString=" + mLockString);
                } else {
                    mLockString = mLockString + "0";
                    Log.d("Value_of_spinner", "mLockString=" + mLockString);
                }

            }

            if (mLockString.contains("L")) {
                mLockStatus = mLockString.substring(0, 4);
                Log.d("Value_of_spinner", "mLockStatus=" + mLockStatus);
            }
            Log.d("Lock Value", mLockStatus);

            if (cbSwitchSelect.isChecked()) {
                mainLock = "1";
                if (mLockStatus.equals("NA")) {
                    Toast.makeText(this, "Select lock while selecting Mainlock", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {

                mainLock = "NA";
            }

            Log.d("Main Lock", "" + mainLock);

            if (curtainIndex.size() >= 2) {
                if (curtainIndex.size() < 3) {
                    int i = curtainIndex.get(curtainIndex.size() - 1);
                    curtainValue = "c" + i;
                    Log.d("Value_of_spinner", "curtainValue" + curtainValue);
                } else {
                    curtainValue = "cc";
                    Log.d("Value_of_spinner", "curtainValue" + curtainValue);
                }

            } else if (curtainIndex.size() == 0) {
                curtainValue = "NA";
            } else {
                Toast.makeText(this, "Select Two curtains...", Toast.LENGTH_SHORT).show();
            }
            Log.d("Curtain", " " + curtainValue);

            String strHomeId = edtHomeID.getText().toString().trim();

            if (strHomeId.equals("")) {
                Toast.makeText(this, "Please enter Home ID.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!curtainValue.equals("NA") && !mLockStatus.equals("NA")) {
                Toast.makeText(this, "Either select lock or curtain", Toast.LENGTH_SHORT).show();
                return;
            }

            if (curtainValue.equals("NA") && !mLockStatus.equals("NA")) {
                curtainValue = mLockStatus;
                Log.d("All Values", "curtainValue" + curtainValue);
            }

            //i made changes here

            if (mIOTProdID.equals("1")) {
                curtainValue = "gc";
            }

//            if(mIOTProdID.equals("2")){
//                curtainValue="gc";
//            }

            Utils.saveSharedP(this, Utils.HOMEID, edtHomeID.getText().toString());
            mFinalString = mSwitchString;
            Log.d(TAG, "onClickOfSubmit: " + mFinalString + "-" + mIOTProdShortCut);

            getClientList(false);

        } else {
            Toast.makeText(this, "Please enter number of switches", Toast.LENGTH_SHORT).show();
        }
    }

    private void AddView(int curPos) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.row_layout, null);

        final Spinner spSwitchType = (Spinner) rowView.findViewById(R.id.spSwitchType);

        ArrayList<HashMap<String, String>> arrSwitchTypeList = new ArrayList<>();
        ArrayList<HashMap<String, String>> arrSwitchTypeListDimm = new ArrayList<>();


        String[] switchArr = getResources().getStringArray(R.array.switch_type);
        String[] switchArrID = getResources().getStringArray(R.array.switch_type_id);

        String[] switchArrDimm = getResources().getStringArray(R.array.switch_type_Dimm);
        String[] switchArrIDDimm = getResources().getStringArray(R.array.switch_type_id_Dimm);

        for (int i = 0; i < switchArr.length; i++) {
            HashMap<String, String> map = new HashMap<>();
            map.put("ID", switchArrID[i]);
            map.put("NAME", switchArr[i]);
            arrSwitchTypeListDimm.add(map);
        }

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(MainActivity.this, arrSwitchTypeListDimm);
        spSwitchType.setAdapter(customSpinnerAdapter);

        //       else{
//            for (int i = 0; i < switchArr.length; i++) {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("ID", switchArrID[i]);
//                map.put("NAME", switchArr[i]);
//                arrSwitchTypeList.add(map);
//            }
//            CustomSpinnerAdapter customSpinnerAdapter=new CustomSpinnerAdapter(MainActivity.this,arrSwitchTypeList);
//            spSwitchType.setAdapter(customSpinnerAdapter);
//        }


        llDynamicSpinner.addView(rowView, llDynamicSpinner.getChildCount() - 1);

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    public ArrayList<HashMap<String, String>> getClientList(boolean onlyReachables) {
        return getClientList(onlyReachables, 300);
    }

    public ArrayList<HashMap<String, String>> getClientList(boolean onlyReachables, int reachableTimeout) {
        BufferedReader br = null;

        try {
            mMapResult = new ArrayList<>();
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                Log.d("line_value", "=" + line);
                String[] splitted = line.split(" +");

                if ((splitted != null) && (splitted.length >= 4)) {
                    // Basic sanity check
                    Log.d("splitted", "=" + splitted[0]);
                    String mac = splitted[3];

                    Log.d("mac", "mac" + mac);
                    if (mac.matches("..:..:..:..:..:..")) {//"c0:c9:76:72:fe:cf"

                        new GetConnectedUser(splitted, onlyReachables).execute();

                    } else {
                        //  Toast.makeText(MainActivity.this, "Device not connected to Hotspot.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), e.getMessage());
            }
        }

        return mMapResult;
    }

    class GetConnectedUser extends AsyncTask<Void, Void, Void> {
        String[] splitted;
        boolean onlyReachables;
        String CurrIP;
        private BufferedReader inFromServer;
        private String mFinalResult;

        public GetConnectedUser(String[] splitted, boolean onlyReachables) {
            this.splitted = splitted;
            this.onlyReachables = onlyReachables;
            CurrIP = splitted[0];
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDailog = new ProgressDialog(MainActivity.this);
            pDailog.setMessage("Please wait...");
            pDailog.setCancelable(true);
            pDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Log.d("testing_value", "ipaddress== " + splitted[0]);
                boolean isReachable = InetAddress.getByName(splitted[0]).isReachable(300);
                Log.d("testing_value", "reachable==" + isReachable);

                if (!onlyReachables || isReachable) {
                    HashMap<String, String> mMap = new HashMap<>();
                    mMap.put("1", splitted[0]);
                    Log.d("splitted_value", "==" + splitted[0]);
                    mMap.put("2", splitted[3]);
                    Log.d("splitted_value", "==" + splitted[3]);
                    mMap.put("3", splitted[5]);
                    Log.d("splitted_value", "==" + splitted[5]);
                    mMapResult.add(mMap);
                }

                if (splitted[0] != null && !splitted[0].equals("")) {

                    Socket pingSocket = null;
                    PrintWriter out = null;

                    BufferedReader in = null;
                    Log.d("testing_value", "inside ");
                    try {
                        Log.d("splitted_value", "==" + splitted[0]);
                        pingSocket = new Socket(splitted[0], 23);

                        out = new PrintWriter(pingSocket.getOutputStream(), true);      //for sending daata //write  //printwriter for text format

                        Log.d("testing_value", "print value======= " + out.toString());
                        Log.d("testing_value", "printwriter value== " + splitted[0]);


                        Log.d("testing_value", "socket value== " + pingSocket.getOutputStream().toString());

                    } catch (final IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (pDailog != null) {
                                    pDailog.cancel();
                                }

                                Toast.makeText(MainActivity.this, "Device not connected.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    if (out != null) {

                        SwitchSessionManager session = new SwitchSessionManager(MainActivity.this);
                        String sid = ":s" + session.getPSSID() + "," + session.getPPASSWORD() + ",/r/n";
                        String pid = ":p" + session.getSSSID() + "," + session.getSPASSWORD() + ",/r/n";

                        out.println(sid);


                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        out.println(pid);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // int panelNumber=11;
                        //  int panelNumberS=1;
                        Log.d("SEND_CONFIG", "panel nmber in send socket==" + panelNumber2);
                        int panelNumber = panelNumber2;
                        int panelNumberS = total_room_size + 1;

                        // response=req.doGetRequest("http://"+Utils.URL_GENIE_AWS+"/home/getLastIotProductNumber");
                      /*  response=req.doGetRequest("http://192.168.0.2:6060/home/getLastIotProductNumber");
                        Log.d("Get_All_Data ","URL==" +"http://192.168.0.2:6060/home/getLastIotProductNumber==="+response);

                        JSONObject jResult=new JSONObject(response);
                        JSONObject lastdigit= (JSONObject) jResult.get("last_iot_product_number");
                        panelNumber= Integer.parseInt(lastdigit.toString());
                        Log.d("Get_All_Data","last value==="+panelNumber);
*/

                        DatabaseHandler db = new DatabaseHandler(MainActivity.this);
                        ArrayList<HashMap<String, String>> map = db.getAllHomeInfo();
                        if (map.size() > 0) {
                            panelNumber = panelNumber + map.size();
                            panelNumberS = panelNumberS + map.size();

                            Log.d("Panel_Number", "size===" + panelNumber);
                            Log.d("Panel_Number", "size===" + panelNumberS);

                        } else {
                            Log.d("check", "null data");
                        }

                        final String switchDeatils = mRoomShortCut + "-" + panelNumber + "-" + mFinalString + "-" + Utils.getSharedPValue(MainActivity.this, Utils.HOMEID);
                        Log.d("Panel_Number", "ALL SWITCH===" + switchDeatils);

                        final String mSwitchInfo = ":c" + switchDeatils + ",/r/n";
                        out.println(mSwitchInfo);

                        Log.d("Switch Info", mSwitchInfo);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        final String panel = "panel" + panelNumberS;

                        final String panelDetails = panel + "," + panel + "/in," + panel + "/out," + session.getTopicName() + ",";

                        final String checkPanelDetails = panel + "-" + panel + "/in-" + panel + "/out-" + session.getTopicName();

                        String mPanelInfo = ":t" + panelDetails + "/r/n";
                        out.println(mPanelInfo);
                        Log.d("Panel Info", mPanelInfo);

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        String mAllInfo = ":f";
                        out.println(mAllInfo);

                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                        }

                        byte[] buffer = new byte[2048];
                        int bytes;
                        InputStream ing = pingSocket.getInputStream();
                        bytes = ing.read(buffer);
                        String readMessage = new String(buffer, 0, bytes);

                        Log.d("MainActivity->", "Message :: " + readMessage);

                        String CurrString = switchDeatils + "-" + session.getPSSID() + "-" + session.getPPASSWORD() + "-" + session.getSSSID() + "-" + session.getSPASSWORD() + "-" + checkPanelDetails;
                        Log.d("Current String", CurrString);

                        if (readMessage != null) {
                            readMessage = readMessage.replaceAll("\\r\\n", "");
                        }

                        Log.d(":F String ", readMessage);

                        if (!CurrString.equals(readMessage)) {
                            System.out.print("Do it again.");
                            out.close();
                            pingSocket.close();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "Missmatch the data.Do it again.", Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            Log.d("Successs", "Write Successfully");
                            out.close();
                            pingSocket.close();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pDailog != null && pDailog.isShowing()) {
                                        pDailog.cancel();
                                    }

                                    if (mIOTProdID.equals("2")) {
                                        DatabaseHandler db = new DatabaseHandler(MainActivity.this);
                                        db.addGenieTower();
                                        llHideLayout.setVisibility(View.VISIBLE);
                                        initIOTProductTypeSpinner();
                                        Toast.makeText(MainActivity.this, "Genie tower added successfully", Toast.LENGTH_LONG).show();
                                    } else {
                                        HashMap<String, String> mapInfo = new HashMap<>();
                                        mapInfo.put(DatabaseHandler.ROOMID, mRoomID);
                                        mapInfo.put(DatabaseHandler.ROOMNAME, mRoomName);
                                        mapInfo.put(DatabaseHandler.ROOM_LOCATION, mRoomLocation);
                                        mapInfo.put(DatabaseHandler.SWITCHIP, splitted[0]);
                                        mapInfo.put(DatabaseHandler.HOMEID, Utils.getSharedPValue(MainActivity.this, Utils.HOMEID));
                                        mapInfo.put(DatabaseHandler.TOPIC, panel);
                                        mapInfo.put(DatabaseHandler.SWITCHDETAILS, switchDeatils);
                                        Log.d("switchdtails_values", "===" + switchDeatils);
                                        mapInfo.put(DatabaseHandler.PANELDEATILS, panelDetails);
                                        mapInfo.put(DatabaseHandler.IOTPRODSHORTCUT, mIOTProdShortCut);
                                        mapInfo.put(DatabaseHandler.CURTAINVALUE, curtainValue);
                                        mapInfo.put(DatabaseHandler.IOTPRODNAME, mIOTProdName);
                                        mapInfo.put(DatabaseHandler.LOCKVALUE, mainLock);
                                        //mapInfo.put(DatabaseHandler.SWITCHLIST, str_switchList);
                                        DatabaseHandler db = new DatabaseHandler(MainActivity.this);

                                        Log.d(TAG, "run: " + mapInfo);
                                        db.addHomeInfo(mSwitchListMap, mapInfo);
                                        Toast.makeText(MainActivity.this, "Data Send Succssfully", Toast.LENGTH_SHORT).show();

                                    }

                                    if (mainLock.equals("1")) {
                                        manager.setMAINLOCK(mainLock);
                                    }

                                    initSwitchLocationSpinner();
                                    initRoomTypeSpinner();
                                    llDynamicSpinner.removeAllViews();
                                    for (int i = 0; i < 6; i++) {
                                        AddView(i);
                                    }

                                    runOnUiThread( new Runnable() {
                                        @Override
                                        public void run() {

                                            if (pDailog != null && pDailog.isShowing()) {
                                                pDailog.cancel();
                                            }
                                        }
                                    });



                                    Intent intent = new Intent(MainActivity.this, PauseHotspot.class);
                                    startActivity(intent);


                                }
                            });
                        }

                    }

                }


            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread( new Runnable() {
                    @Override
                    public void run() {

                        if (pDailog != null && pDailog.isShowing()) {
                            pDailog.cancel();
                        }

                        Toast.makeText(MainActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDailog != null && pDailog.isShowing()) {
                pDailog.cancel();
            }

            //Toast.makeText(MainActivity.this,"Please try again.",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item1:
                Intent i = new Intent(this, RoomListAcitivity.class);
                startActivity(i);
                return true;
//            case R.id.item4:
//                Intent intentpass=new Intent(MainActivity.this,WifiPassword.class);
//                startActivity(intentpass);
//                return true;
            case R.id.item2:
                Utils.saveSharedP(this, Utils.HOME_FLAG, "");
                Utils.saveSharedP(this, Utils.HOMEID, "");
                DatabaseHandler db = new DatabaseHandler(this);
                db.deleteHomeTable();

                //reshma made changes here
                db.deleteTowerTable();
                //

                Intent intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return true;
            case R.id.item3:
                Intent intent1 = new Intent(MainActivity.this, SendConfiguration.class);
                startActivity(intent1);
                return true;

            case R.id.item5:

                Intent intentReCong = new Intent(MainActivity.this, ReconfigActivity.class);
                startActivity(intentReCong);
                return true;

            case R.id.item6:
                startActivity(new Intent(this, ChangePanelTopic.class));
                return true;


//                WifiManager wifiManager = (WifiManager)this.getSystemService(Context.WIFI_SERVICE);
//                wifiManager.setWifiEnabled(true);
//                DatabaseHandler database=new DatabaseHandler(MainActivity.this);
//                panelList=database.getAllHomeInfo();
//                String mPanelList="";
//                for(int pn=0;pn<panelList.size();pn++){
//                    if(pn==panelList.size()-1){
//                        mPanelList=mPanelList+panelList.get(pn).get(DatabaseHandler.TOPIC)+"-"+panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS);
//                    } else{
//                        mPanelList=mPanelList+panelList.get(pn).get(DatabaseHandler.TOPIC)+"-"+panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS)+",";
//                    }
//
//                }
//
//                SwitchSessionManager session=new SwitchSessionManager(MainActivity.this);
//                String mRouter=session.getPSSID()+","+session.getPPASSWORD();
//                String mSecure=session.getSSSID()+","+session.getSPASSWORD();
//
//                String mFinal="";
//                for(int pp=0;pp<panelList.size();pp++){
//                    mFinal=mFinal+panelList.get(pp).get(DatabaseHandler.SWITCHDETAILS)+","+mRouter+","+mSecure+","+panelList.get(pp).get(DatabaseHandler.PANELDEATILS)+",";
//                    Log.d("ConfigDetails",mFinal);
//                }
//
//                Log.d("Panel List-->",mPanelList);
//
//                if(panelList.size()>0) {
//                    new AsynConfigureFile().execute(mPanelList, mFinal);
//                }else{
//                    Toast.makeText(this, "Fill the Panel.", Toast.LENGTH_SHORT).show();
//                }
//
//                return true;
        }

        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        turnOffHotspot();
     }

    public void turnOffHotspot() {

        wifimanager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);

        try {
            Method[] wmMethods = wifimanager.getClass().getDeclaredMethods();
            WifiConfiguration wifiConfiguration = new WifiConfiguration();

            for (Method method : wmMethods) {
                if (method.getName().equals("setWifiApEnabled")) {
                    try {
                        method.invoke(this.wifimanager, wifiConfiguration, false);

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            Log.d("Hotspot", "Hotspot turn off");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    private class Lastproductid extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... voids) {

            try {
                JSONObject jObj = new JSONObject();
                jObj.put("homeId", manager.getHOMEID());

                //{"last_iot_product_number":"55","total_room_size":9,"last_Switch_Id":234}
                response = req.doPostRequest(URL_GENIE_AWS + "/home/getLastIotProductNumber", "" + jObj);
                Log.d("SEND_CONFIG", "last value===" + response);
                JSONObject jResult = new JSONObject(response);
                Object lastdigit = jResult.get("last_iot_product_number");
                panelNumber3 = Integer.parseInt(lastdigit.toString());
                Object roomsize = jResult.get("total_room_size");
                total_room_size = Integer.parseInt(roomsize.toString());
                Object last_switch_id = jResult.get("last_Switch_Id");
                last_Switch_Id = Integer.parseInt(last_switch_id.toString());
                last_Switch_Id1 = last_Switch_Id;

                panelNumber2 = panelNumber3 + 1;
                Log.d("SEND_CONFIG", "last value1===" + panelNumber2);
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("SEND_CONFIG", "error===" + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("SEND_CONFIG", "error====" + e.getMessage());
            }
            // Log.d("Get_All_Data ","URL==" +"http://192.168.0.2:6060/home/getLastIotProductNumber==="+response);
            return panelNumber2;
        }
    }
}
