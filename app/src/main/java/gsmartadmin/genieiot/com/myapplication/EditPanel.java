package gsmartadmin.genieiot.com.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;

import gsmartadmin.genieiot.com.myapplication.Adapter.CustomSpinnerAdapter;
import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

import static gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler.SWITCHDETAILS;

/**
 * Created by root on 12/5/17.
 */

public class EditPanel extends Activity implements AdapterView.OnItemSelectedListener,View.OnClickListener{

    ArrayList<HashMap<String,String>> map;
    private Spinner spRoomType,spSwitchLocation,spIotProductType;
    private Button btnEnter,btnSubmit;
    private LinearLayout parentLayout,llDynamicSpinner;
    private EditText edtSwitchNo,edtHomeID;
    private String mRoomName="",mRoomLocation="",mRoomID="",mRoomLocationID="",mRoomShortCut="",mIOTProdName="",mIOTProdID="",mIOTProdShortCut="";
    private String mFinalString;
    private WifiManager wifimanager;
    private ProgressDialog pDailog;
    private ArrayList<HashMap<String,String>> mSwitchListMap;
    private ArrayList<HashMap<String, String>> panelList;
    private ProgressDialog progressDialog;
    HashMap<String,String> mMap;
    SwitchSessionManager manager;
    DatabaseHandler handler;
    String curtainValue="";
    private ArrayList<HashMap<String,String>> arrRoomList,arrSwitchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeControls();

        Intent intent=getIntent();
         HashMap<String,String>  maplist= (HashMap<String, String>) getIntent().getSerializableExtra("CONFIGURATION_DETAILS");
        maplist.get(SWITCHDETAILS);


        Log.d("Selected Position Data",""+maplist);

       // HashMap<String, String> map = new HashMap<String, String>();

        edtHomeID.setText(Utils.getSharedPValue(this,Utils.HOMEID));


       // CustomSpinnerAdapter customSpinnerAdapter=new CustomSpinnerAdapter(EditPanel.this, maplist.get("iot_prod_shortcut"));
       // spIotProductType.setAdapter(customSpinnerAdapter);



    }

    private void initializeControls()
    {
        panelList=new ArrayList<>();
        arrRoomList=new ArrayList<>();
        arrSwitchList=new ArrayList<>();

        parentLayout = (LinearLayout)findViewById(R.id.parentLayout);
        llDynamicSpinner = (LinearLayout)findViewById(R.id.llDynamicSpinner);
        spRoomType= (Spinner) findViewById(R.id.spRoomType);
        spSwitchLocation= (Spinner) findViewById(R.id.spSwitchLocation);
        btnEnter= (Button) findViewById(R.id.btnEnter);
        btnSubmit= (Button) findViewById(R.id.btnSubmit);
        edtSwitchNo= (EditText) findViewById(R.id.edtSwitchNo);
        edtHomeID= (EditText) findViewById(R.id.edtHomeID);
        spIotProductType= (Spinner) findViewById(R.id.spIotProductType);

        handler=new DatabaseHandler(this);
        manager=new SwitchSessionManager(this);

        spRoomType.setOnItemSelectedListener(this);
        spSwitchLocation.setOnItemSelectedListener(this);
        spIotProductType.setOnItemSelectedListener(this);
        btnEnter.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        llDynamicSpinner.removeAllViews();
    }

    @Override
    public void onClick(View v)
    {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
